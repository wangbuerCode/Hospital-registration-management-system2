package com.xhu.dao;

import com.xhu.entity.User;
import com.xhu.entity.UserExample;
import com.xhu.util.BootPage;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
	int countByExample(UserExample example);
	int countByExample1(@Param("seacher")String seacher);
	int deleteByExample(UserExample example);

	int deleteByPrimaryKey(Integer userId);

	int insert(User record);

	int insertSelective(User record);

	List<User> selectByExample(UserExample example);
	/**
	 * 
	 * @Title: selectByExample1 
	 * @Description: 获取用户列表
	 * @param example
	 * @param page
	 * @param seacher
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月26日 下午1:34:40 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<User> selectByExample1(UserExample example,@Param("page")BootPage<User> page,@Param("seacher")String seacher);
	User selectByPrimaryKey(Integer userId);

	int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

	int updateByExample(@Param("record") User record, @Param("example") UserExample example);

	int updateByPrimaryKeySelective(User record);

	int updateByPrimaryKey(User record);
	/**
	 * 
	 * @Title: uppwd 
	 * @Description: 医生首次登陆修改密码 
	 * @param pid
	 * @param pwd
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月26日 下午1:30:21 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	int uppwd(@Param("id") String pid, @Param("pwd") String pwd);
}