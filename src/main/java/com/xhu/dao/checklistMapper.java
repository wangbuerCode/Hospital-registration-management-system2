package com.xhu.dao;

import com.xhu.entity.checklist;
import com.xhu.entity.checklistExample;
import com.xhu.util.BootPage;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface checklistMapper {
    int countByExample(checklistExample example);
    int countByExample1(@Param("doctorid")String doctorid,@Param("search")String search);
    int deleteByExample(checklistExample example);

    int deleteByPrimaryKey(Integer checkId);

    int insert(checklist record);

    int insertSelective(checklist record);

    List<checklist> selectByExample(@Param("page")BootPage<checklist> page,checklistExample example,@Param("name")String name);
    
    List<checklist> selectByExample1(checklistExample example ,@Param("page")BootPage<checklist> page,@Param("doctorid")String doctorid,@Param("search")String search);
    checklist selectByPrimaryKey(Integer checkId);

    int updateByExampleSelective(@Param("record") checklist record, @Param("example") checklistExample example);

    int updateByExample(@Param("record") checklist record, @Param("example") checklistExample example);

    int updateByPrimaryKeySelective(checklist record);

    int updateByPrimaryKey(checklist record);
}