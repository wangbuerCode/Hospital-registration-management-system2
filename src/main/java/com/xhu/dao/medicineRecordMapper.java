package com.xhu.dao;

import com.xhu.entity.medicineRecord;
import com.xhu.entity.medicineRecordExample;
import com.xhu.util.BootPage;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface medicineRecordMapper {
    int countByExample(medicineRecordExample example);
    int countByExample1();
    int deleteByExample(medicineRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(medicineRecord record);

    int insertSelective(medicineRecord record);

    List<medicineRecord> selectByExample(@Param("page")BootPage<medicineRecord> page);

    medicineRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") medicineRecord record, @Param("example") medicineRecordExample example);

    int updateByExample(@Param("record") medicineRecord record, @Param("example") medicineRecordExample example);

    int updateByPrimaryKeySelective(medicineRecord record);

    int updateByPrimaryKey(medicineRecord record);
    /**
     * 
     * @Title: ilnum 
     * @Description: 获取药品数据 
     * @return
     * @author lb  lb  
     * @date 2019年3月11日 下午7:44:06 
     * @修改信息 (修改人修改内容及修改时间)
     */
    List<Map<String,String>> ilnum();
}