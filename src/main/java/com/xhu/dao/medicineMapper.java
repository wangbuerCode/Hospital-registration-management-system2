package com.xhu.dao;

import com.xhu.entity.medicine;
import com.xhu.entity.medicineExample;
import com.xhu.util.BootPage;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface medicineMapper {
    int countByExample(@Param("sp")String sp);

    int deleteByExample(medicineExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(medicine record);

    int insertSelective(medicine record);

    List<medicine> selectByExample(medicineExample example,@Param("page")BootPage<medicine> page,@Param("sp")String sp);

    medicine selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") medicine record, @Param("example") medicineExample example);

    int updateByExample(@Param("record") medicine record, @Param("example") medicineExample example);

    int updateByPrimaryKeySelective(medicine record);

    int updateByPrimaryKey(medicine record);
    void insert1();
    void de();
}