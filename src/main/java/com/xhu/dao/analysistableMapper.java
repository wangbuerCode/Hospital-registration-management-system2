package com.xhu.dao;

import com.xhu.entity.analysistable;
import com.xhu.entity.analysistableExample;
import com.xhu.util.BootPage;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface analysistableMapper {
	int countByExample(analysistableExample example);

	int deleteByExample(analysistableExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(analysistable record);

	int insertSelective(analysistable record);

	List<analysistable> selectByExample(analysistableExample example);

	analysistable selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") analysistable record, @Param("example") analysistableExample example);

	int updateByExample(@Param("record") analysistable record, @Param("example") analysistableExample example);

	int updateByPrimaryKeySelective(analysistable record);

	int updateByPrimaryKey(analysistable record);

	/**
	 * 
	 * @Title: listFa
	 * @Description: TODO(获取每年的发病率)
	 * @param page
	 * @return
	 * @author lb lb
	 * @date 2019年1月6日 下午1:06:31
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> listFa(@Param("year") String year, @Param("name") String name);

	/**
	 * 
	 * @Title: getsex
	 * @Description: TODO(获取每月每种疾病的人数)
	 * @return
	 * @author lb lb
	 * @date 2019年1月6日 下午3:13:06
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getsex(@Param("year") String year, @Param("ilname") String ilname);

	/**
	 * 
	 * @Title: getagesex
	 * @Description: TODO(每个年龄段患病的男女比列)
	 * @return
	 * @author lb lb
	 * @date 2019年1月6日 下午5:24:18
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getagesex(@Param("year") String year, @Param("ilname") String ilname);

	/**
	 * 
	 * @Title: getEveryYear
	 * @Description: TODO(统计某种疾病在每一年中的变化)
	 * @return
	 * @author lb lb
	 * @date 2019年1月7日 下午2:58:10
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getEveryYear(@Param("ilname") String ilname);

	/**
	 * 
	 * @Title: getMouth
	 * @Description: TODO(获取月份)
	 * @return
	 * @author lb lb
	 * @date 2019年1月8日 上午9:07:58
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getMouth();

	/**
	 * 
	 * @Title: getstudent
	 * @Description: TODO(获取2016年的学生患感冒的学生)
	 * @return
	 * @author lb lb
	 * @date 2019年1月8日 上午11:12:24
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getstudent();

	/**
	 * 
	 * @Title: getstudent1
	 * @Description: 获取2017患感冒的学生人数
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:12:43
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getstudent1();

	/**
	 * 
	 * @Title: getstudent2
	 * @Description: 获取2018患感冒的学生人数
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:13:20
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getstudent2();

	/**
	 * 
	 * @Title: getstudent3
	 * @Description: 获取2019患感冒的学生人数
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:13:40
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getstudent3();

	/**
	 * 
	 * @Title: getfarmer
	 * @Description: 获取2016患感冒的农民人数
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:30:29
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getfarmer();

	/**
	 * 
	 * @Title: getfarmer
	 * @Description: 获取2017患感冒的农民人数
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:30:29
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getfarmer1();

	/**
	 * 
	 * @Title: getfarmer
	 * @Description: 获取2018患感冒的农民人数
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:30:29
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getfarmer2();

	/**
	 * 
	 * @Title: getfarmer
	 * @Description: 获取2019患感冒的农民人数
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:30:29
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getfarmer3();

	/**
	 * 
	 * @Title: getwhite
	 * @Description: 从2016-2019患感冒的白领人数
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:50:19
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getwhite();

	List<Map<String, String>> getwhite1();

	List<Map<String, String>> getwhite2();

	List<Map<String, String>> getwhite3();

	/**
	 * 
	 * @Title: getoilNumberAndIndex
	 * @Description: 統計某种疾病在年龄段的数量和发病率
	 * @return
	 * @author lb lb
	 * @date 2019年1月11日 下午1:10:46
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> getoilNumberAndIndex(@Param("ilname") String ilname, @Param("year") String year);

	/**
	 * 
	 * @Title: mouthData
	 * @Description: 获得年度报表数据
	 * @return
	 * @author lb lb
	 * @date 2019年3月10日 下午4:28:50
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	List<Map<String, String>> yearData(@Param("year")String year);
}