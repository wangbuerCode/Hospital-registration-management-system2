package com.xhu.dao;

import com.xhu.entity.treatmentrecord;
import com.xhu.entity.treatmentrecordExample;
import com.xhu.util.BootPage;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface treatmentrecordMapper {
    int countByExample(treatmentrecordExample example);
    int countByExample1(@Param("search")String search);
    int deleteByExample(treatmentrecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(treatmentrecord record);

    int insertSelective(treatmentrecord record);

    List<treatmentrecord> selectByExample(treatmentrecordExample example);
    
    List<treatmentrecord> selectByExample1(treatmentrecordExample example,@Param("page")BootPage<treatmentrecord> page,@Param("search")String search);
    treatmentrecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") treatmentrecord record, @Param("example") treatmentrecordExample example);

    int updateByExample(@Param("record") treatmentrecord record, @Param("example") treatmentrecordExample example);

    int updateByPrimaryKeySelective(treatmentrecord record);

    int updateByPrimaryKey(treatmentrecord record);
}