package com.xhu.dao;

import com.xhu.entity.ifodoctor;
import com.xhu.entity.ifodoctorExample;
import com.xhu.util.BootPage;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ifodoctorMapper {
	int countByExample();
	int countByExample1(@Param("search")String search);
	int deleteByExample(ifodoctorExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(ifodoctor record);

	int insertSelective(ifodoctor record);

	List<ifodoctor> selectByExample(ifodoctorExample example, @Param("page") BootPage<ifodoctor> page,@Param("name")String name);
	List<ifodoctor> selectByExample1(ifodoctorExample example, @Param("page") BootPage<ifodoctor> page,@Param("search")String search);
	ifodoctor selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") ifodoctor record, @Param("example") ifodoctorExample example);

	int updateByExample(@Param("record") ifodoctor record, @Param("example") ifodoctorExample example);

	int updateByPrimaryKeySelective(ifodoctor record);

	int updateByPrimaryKey(ifodoctor record);
}