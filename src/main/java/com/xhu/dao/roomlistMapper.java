package com.xhu.dao;

import com.xhu.entity.roomlist;
import com.xhu.entity.roomlistExample;
import com.xhu.util.BootPage;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface roomlistMapper {
    int countByExample(roomlistExample example);
    
    int countByExample1(@Param("search")String search);
    int deleteByExample(roomlistExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(roomlist record);

    int insertSelective(roomlist record);

    List<roomlist> selectByExample(roomlistExample example);
    
    List<roomlist> selectByExample1(roomlistExample example,@Param("page")BootPage<roomlist> page,@Param("search")String search);
    
    roomlist selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") roomlist record, @Param("example") roomlistExample example);

    int updateByExample(@Param("record") roomlist record, @Param("example") roomlistExample example);

    int updateByPrimaryKeySelective(roomlist record);

    int updateByPrimaryKey(roomlist record);
}