package com.xhu.dao;

import com.xhu.entity.orderlist;
import com.xhu.entity.orderlistExample;
import com.xhu.util.BootPage;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface orderlistMapper {
    int countByExample(orderlistExample example);

    int deleteByExample(orderlistExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(orderlist record);

    int insertSelective(orderlist record);

    List<orderlist> selectByExample(orderlistExample example);

    orderlist selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") orderlist record, @Param("example") orderlistExample example);

    int updateByExample(@Param("record") orderlist record, @Param("example") orderlistExample example);

    int updateByPrimaryKeySelective(orderlist record);

    int updateByPrimaryKey(orderlist record);
    
    List<orderlist> selectByExample1(@Param("page")BootPage<orderlist> page,@Param("name")String name,@Param("search")String search);
    int countByExample1(@Param("id")String name,@Param("search")String search);

	List<orderlist> selectByExample2(@Param("page")BootPage<orderlist> page, @Param("name")String name);

	int countByExample2(@Param("id")String name);
	/**
	 * 
	 * @Title: getAllNumber 
	 * @Description: 获取医生的每天的预约人数
	 * @return
	 * @author lb  lb  
	 * @date 2019年6月1日 下午5:52:29 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	int getAllNumber(@Param("doctorId")String doctorId,@Param("date") Date date);
	/**
	 * 
	 * @Title: twonumber 
	 * @Description: 设置不能重复的预约
	 * @return
	 * @author lb  lb  
	 * @date 2019年6月1日 下午5:55:13 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	int twonumber(@Param("doctorId")String doctorId,@Param("date") Date date,@Param("username")String username);
}