package com.xhu.dao;

import com.xhu.entity.medicineMedia;
import com.xhu.entity.medicineMediaExample;
import com.xhu.util.BootPage;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface medicineMediaMapper {
    int countByExample();

    int deleteByExample(medicineMediaExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(medicineMedia record);

    int insertSelective(medicineMedia record);

    List<medicineMedia> selectByExample(medicineMediaExample example,@Param("page")BootPage<medicineMedia> page);

    medicineMedia selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") medicineMedia record, @Param("example") medicineMediaExample example);

    int updateByExample(@Param("record") medicineMedia record, @Param("example") medicineMediaExample example);

    int updateByPrimaryKeySelective(medicineMedia record);

    int updateByPrimaryKey(medicineMedia record);
    void del();
}