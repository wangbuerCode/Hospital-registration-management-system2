package com.xhu.entity;

import java.io.Serializable;
import java.util.Date;

public class treatmentrecord implements Serializable {
    private Integer id;

    private String doctorId;

    private String sickId;

    private Date recordTime;

    private String recordRoom;

    private String recordResult;

    private String careUse;

    private String sex;

    private Integer age;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getSickId() {
        return sickId;
    }

    public void setSickId(String sickId) {
        this.sickId = sickId;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public String getRecordRoom() {
        return recordRoom;
    }

    public void setRecordRoom(String recordRoom) {
        this.recordRoom = recordRoom;
    }

    public String getRecordResult() {
        return recordResult;
    }

    public void setRecordResult(String recordResult) {
        this.recordResult = recordResult;
    }

    public String getCareUse() {
        return careUse;
    }

    public void setCareUse(String careUse) {
        this.careUse = careUse;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}