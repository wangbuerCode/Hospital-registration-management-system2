package com.xhu.entity;

import java.io.Serializable;
import java.util.Date;

public class news implements Serializable {
    private Integer id;

    private String publishName;

    private Date publishTime;

    private String publishContext;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPublishName() {
        return publishName;
    }

    public void setPublishName(String publishName) {
        this.publishName = publishName;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public String getPublishContext() {
        return publishContext;
    }

    public void setPublishContext(String publishContext) {
        this.publishContext = publishContext;
    }
}