package com.xhu.entity;

import java.io.Serializable;
import java.util.Date;

public class medicine implements Serializable {
    private Integer id;

    private String mnoid;

    private String mname;

    private String mspecs;

    private String munit;

    private Integer mprice;

    private Date mdate;

    private Integer mval;

    private Integer mnumber;

    private String sno;

    private String iscf;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMnoid() {
        return mnoid;
    }

    public void setMnoid(String mnoid) {
        this.mnoid = mnoid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getMspecs() {
        return mspecs;
    }

    public void setMspecs(String mspecs) {
        this.mspecs = mspecs;
    }

    public String getMunit() {
        return munit;
    }

    public void setMunit(String munit) {
        this.munit = munit;
    }

    public Integer getMprice() {
        return mprice;
    }

    public void setMprice(Integer mprice) {
        this.mprice = mprice;
    }

    public Date getMdate() {
        return mdate;
    }

    public void setMdate(Date mdate) {
        this.mdate = mdate;
    }

    public Integer getMval() {
        return mval;
    }

    public void setMval(Integer mval) {
        this.mval = mval;
    }

    public Integer getMnumber() {
        return mnumber;
    }

    public void setMnumber(Integer mnumber) {
        this.mnumber = mnumber;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getIscf() {
        return iscf;
    }

    public void setIscf(String iscf) {
        this.iscf = iscf;
    }
}