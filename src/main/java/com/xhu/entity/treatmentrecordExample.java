package com.xhu.entity;

import com.xhu.util.BootPage;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class treatmentrecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected BootPage page;

    public treatmentrecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(BootPage page) {
        this.page=page;
    }

    public BootPage getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDoctorIdIsNull() {
            addCriterion("DOCTOR_ID is null");
            return (Criteria) this;
        }

        public Criteria andDoctorIdIsNotNull() {
            addCriterion("DOCTOR_ID is not null");
            return (Criteria) this;
        }

        public Criteria andDoctorIdEqualTo(String value) {
            addCriterion("DOCTOR_ID =", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdNotEqualTo(String value) {
            addCriterion("DOCTOR_ID <>", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdGreaterThan(String value) {
            addCriterion("DOCTOR_ID >", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdGreaterThanOrEqualTo(String value) {
            addCriterion("DOCTOR_ID >=", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdLessThan(String value) {
            addCriterion("DOCTOR_ID <", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdLessThanOrEqualTo(String value) {
            addCriterion("DOCTOR_ID <=", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdLike(String value) {
            addCriterion("DOCTOR_ID like", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdNotLike(String value) {
            addCriterion("DOCTOR_ID not like", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdIn(List<String> values) {
            addCriterion("DOCTOR_ID in", values, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdNotIn(List<String> values) {
            addCriterion("DOCTOR_ID not in", values, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdBetween(String value1, String value2) {
            addCriterion("DOCTOR_ID between", value1, value2, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdNotBetween(String value1, String value2) {
            addCriterion("DOCTOR_ID not between", value1, value2, "doctorId");
            return (Criteria) this;
        }

        public Criteria andSickIdIsNull() {
            addCriterion("SICK_ID is null");
            return (Criteria) this;
        }

        public Criteria andSickIdIsNotNull() {
            addCriterion("SICK_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSickIdEqualTo(String value) {
            addCriterion("SICK_ID =", value, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdNotEqualTo(String value) {
            addCriterion("SICK_ID <>", value, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdGreaterThan(String value) {
            addCriterion("SICK_ID >", value, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdGreaterThanOrEqualTo(String value) {
            addCriterion("SICK_ID >=", value, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdLessThan(String value) {
            addCriterion("SICK_ID <", value, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdLessThanOrEqualTo(String value) {
            addCriterion("SICK_ID <=", value, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdLike(String value) {
            addCriterion("SICK_ID like", value, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdNotLike(String value) {
            addCriterion("SICK_ID not like", value, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdIn(List<String> values) {
            addCriterion("SICK_ID in", values, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdNotIn(List<String> values) {
            addCriterion("SICK_ID not in", values, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdBetween(String value1, String value2) {
            addCriterion("SICK_ID between", value1, value2, "sickId");
            return (Criteria) this;
        }

        public Criteria andSickIdNotBetween(String value1, String value2) {
            addCriterion("SICK_ID not between", value1, value2, "sickId");
            return (Criteria) this;
        }

        public Criteria andRecordTimeIsNull() {
            addCriterion("RECORD_TIME is null");
            return (Criteria) this;
        }

        public Criteria andRecordTimeIsNotNull() {
            addCriterion("RECORD_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andRecordTimeEqualTo(Date value) {
            addCriterionForJDBCDate("RECORD_TIME =", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("RECORD_TIME <>", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("RECORD_TIME >", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("RECORD_TIME >=", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeLessThan(Date value) {
            addCriterionForJDBCDate("RECORD_TIME <", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("RECORD_TIME <=", value, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeIn(List<Date> values) {
            addCriterionForJDBCDate("RECORD_TIME in", values, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("RECORD_TIME not in", values, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("RECORD_TIME between", value1, value2, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("RECORD_TIME not between", value1, value2, "recordTime");
            return (Criteria) this;
        }

        public Criteria andRecordRoomIsNull() {
            addCriterion("RECORD_ROOM is null");
            return (Criteria) this;
        }

        public Criteria andRecordRoomIsNotNull() {
            addCriterion("RECORD_ROOM is not null");
            return (Criteria) this;
        }

        public Criteria andRecordRoomEqualTo(String value) {
            addCriterion("RECORD_ROOM =", value, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomNotEqualTo(String value) {
            addCriterion("RECORD_ROOM <>", value, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomGreaterThan(String value) {
            addCriterion("RECORD_ROOM >", value, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomGreaterThanOrEqualTo(String value) {
            addCriterion("RECORD_ROOM >=", value, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomLessThan(String value) {
            addCriterion("RECORD_ROOM <", value, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomLessThanOrEqualTo(String value) {
            addCriterion("RECORD_ROOM <=", value, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomLike(String value) {
            addCriterion("RECORD_ROOM like", value, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomNotLike(String value) {
            addCriterion("RECORD_ROOM not like", value, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomIn(List<String> values) {
            addCriterion("RECORD_ROOM in", values, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomNotIn(List<String> values) {
            addCriterion("RECORD_ROOM not in", values, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomBetween(String value1, String value2) {
            addCriterion("RECORD_ROOM between", value1, value2, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordRoomNotBetween(String value1, String value2) {
            addCriterion("RECORD_ROOM not between", value1, value2, "recordRoom");
            return (Criteria) this;
        }

        public Criteria andRecordResultIsNull() {
            addCriterion("RECORD_RESULT is null");
            return (Criteria) this;
        }

        public Criteria andRecordResultIsNotNull() {
            addCriterion("RECORD_RESULT is not null");
            return (Criteria) this;
        }

        public Criteria andRecordResultEqualTo(String value) {
            addCriterion("RECORD_RESULT =", value, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultNotEqualTo(String value) {
            addCriterion("RECORD_RESULT <>", value, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultGreaterThan(String value) {
            addCriterion("RECORD_RESULT >", value, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultGreaterThanOrEqualTo(String value) {
            addCriterion("RECORD_RESULT >=", value, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultLessThan(String value) {
            addCriterion("RECORD_RESULT <", value, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultLessThanOrEqualTo(String value) {
            addCriterion("RECORD_RESULT <=", value, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultLike(String value) {
            addCriterion("RECORD_RESULT like", value, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultNotLike(String value) {
            addCriterion("RECORD_RESULT not like", value, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultIn(List<String> values) {
            addCriterion("RECORD_RESULT in", values, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultNotIn(List<String> values) {
            addCriterion("RECORD_RESULT not in", values, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultBetween(String value1, String value2) {
            addCriterion("RECORD_RESULT between", value1, value2, "recordResult");
            return (Criteria) this;
        }

        public Criteria andRecordResultNotBetween(String value1, String value2) {
            addCriterion("RECORD_RESULT not between", value1, value2, "recordResult");
            return (Criteria) this;
        }

        public Criteria andCareUseIsNull() {
            addCriterion("CARE_USE is null");
            return (Criteria) this;
        }

        public Criteria andCareUseIsNotNull() {
            addCriterion("CARE_USE is not null");
            return (Criteria) this;
        }

        public Criteria andCareUseEqualTo(String value) {
            addCriterion("CARE_USE =", value, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseNotEqualTo(String value) {
            addCriterion("CARE_USE <>", value, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseGreaterThan(String value) {
            addCriterion("CARE_USE >", value, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseGreaterThanOrEqualTo(String value) {
            addCriterion("CARE_USE >=", value, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseLessThan(String value) {
            addCriterion("CARE_USE <", value, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseLessThanOrEqualTo(String value) {
            addCriterion("CARE_USE <=", value, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseLike(String value) {
            addCriterion("CARE_USE like", value, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseNotLike(String value) {
            addCriterion("CARE_USE not like", value, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseIn(List<String> values) {
            addCriterion("CARE_USE in", values, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseNotIn(List<String> values) {
            addCriterion("CARE_USE not in", values, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseBetween(String value1, String value2) {
            addCriterion("CARE_USE between", value1, value2, "careUse");
            return (Criteria) this;
        }

        public Criteria andCareUseNotBetween(String value1, String value2) {
            addCriterion("CARE_USE not between", value1, value2, "careUse");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("SEX is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("SEX is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(String value) {
            addCriterion("SEX =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(String value) {
            addCriterion("SEX <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(String value) {
            addCriterion("SEX >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(String value) {
            addCriterion("SEX >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(String value) {
            addCriterion("SEX <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(String value) {
            addCriterion("SEX <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLike(String value) {
            addCriterion("SEX like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotLike(String value) {
            addCriterion("SEX not like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<String> values) {
            addCriterion("SEX in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<String> values) {
            addCriterion("SEX not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(String value1, String value2) {
            addCriterion("SEX between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(String value1, String value2) {
            addCriterion("SEX not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andAgeIsNull() {
            addCriterion("age is null");
            return (Criteria) this;
        }

        public Criteria andAgeIsNotNull() {
            addCriterion("age is not null");
            return (Criteria) this;
        }

        public Criteria andAgeEqualTo(Integer value) {
            addCriterion("age =", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotEqualTo(Integer value) {
            addCriterion("age <>", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThan(Integer value) {
            addCriterion("age >", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThanOrEqualTo(Integer value) {
            addCriterion("age >=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThan(Integer value) {
            addCriterion("age <", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThanOrEqualTo(Integer value) {
            addCriterion("age <=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeIn(List<Integer> values) {
            addCriterion("age in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotIn(List<Integer> values) {
            addCriterion("age not in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeBetween(Integer value1, Integer value2) {
            addCriterion("age between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotBetween(Integer value1, Integer value2) {
            addCriterion("age not between", value1, value2, "age");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}