package com.xhu.entity;

import com.xhu.util.BootPage;
import java.util.ArrayList;
import java.util.List;

public class roomlistExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected BootPage page;

    public roomlistExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(BootPage page) {
        this.page=page;
    }

    public BootPage getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andRoomIdIsNull() {
            addCriterion("ROOM_ID is null");
            return (Criteria) this;
        }

        public Criteria andRoomIdIsNotNull() {
            addCriterion("ROOM_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRoomIdEqualTo(String value) {
            addCriterion("ROOM_ID =", value, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdNotEqualTo(String value) {
            addCriterion("ROOM_ID <>", value, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdGreaterThan(String value) {
            addCriterion("ROOM_ID >", value, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdGreaterThanOrEqualTo(String value) {
            addCriterion("ROOM_ID >=", value, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdLessThan(String value) {
            addCriterion("ROOM_ID <", value, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdLessThanOrEqualTo(String value) {
            addCriterion("ROOM_ID <=", value, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdLike(String value) {
            addCriterion("ROOM_ID like", value, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdNotLike(String value) {
            addCriterion("ROOM_ID not like", value, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdIn(List<String> values) {
            addCriterion("ROOM_ID in", values, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdNotIn(List<String> values) {
            addCriterion("ROOM_ID not in", values, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdBetween(String value1, String value2) {
            addCriterion("ROOM_ID between", value1, value2, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomIdNotBetween(String value1, String value2) {
            addCriterion("ROOM_ID not between", value1, value2, "roomId");
            return (Criteria) this;
        }

        public Criteria andRoomNameIsNull() {
            addCriterion("ROOM_NAME is null");
            return (Criteria) this;
        }

        public Criteria andRoomNameIsNotNull() {
            addCriterion("ROOM_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andRoomNameEqualTo(String value) {
            addCriterion("ROOM_NAME =", value, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameNotEqualTo(String value) {
            addCriterion("ROOM_NAME <>", value, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameGreaterThan(String value) {
            addCriterion("ROOM_NAME >", value, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameGreaterThanOrEqualTo(String value) {
            addCriterion("ROOM_NAME >=", value, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameLessThan(String value) {
            addCriterion("ROOM_NAME <", value, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameLessThanOrEqualTo(String value) {
            addCriterion("ROOM_NAME <=", value, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameLike(String value) {
            addCriterion("ROOM_NAME like", value, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameNotLike(String value) {
            addCriterion("ROOM_NAME not like", value, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameIn(List<String> values) {
            addCriterion("ROOM_NAME in", values, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameNotIn(List<String> values) {
            addCriterion("ROOM_NAME not in", values, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameBetween(String value1, String value2) {
            addCriterion("ROOM_NAME between", value1, value2, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomNameNotBetween(String value1, String value2) {
            addCriterion("ROOM_NAME not between", value1, value2, "roomName");
            return (Criteria) this;
        }

        public Criteria andRoomTelIsNull() {
            addCriterion("ROOM_TEL is null");
            return (Criteria) this;
        }

        public Criteria andRoomTelIsNotNull() {
            addCriterion("ROOM_TEL is not null");
            return (Criteria) this;
        }

        public Criteria andRoomTelEqualTo(String value) {
            addCriterion("ROOM_TEL =", value, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelNotEqualTo(String value) {
            addCriterion("ROOM_TEL <>", value, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelGreaterThan(String value) {
            addCriterion("ROOM_TEL >", value, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelGreaterThanOrEqualTo(String value) {
            addCriterion("ROOM_TEL >=", value, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelLessThan(String value) {
            addCriterion("ROOM_TEL <", value, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelLessThanOrEqualTo(String value) {
            addCriterion("ROOM_TEL <=", value, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelLike(String value) {
            addCriterion("ROOM_TEL like", value, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelNotLike(String value) {
            addCriterion("ROOM_TEL not like", value, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelIn(List<String> values) {
            addCriterion("ROOM_TEL in", values, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelNotIn(List<String> values) {
            addCriterion("ROOM_TEL not in", values, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelBetween(String value1, String value2) {
            addCriterion("ROOM_TEL between", value1, value2, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomTelNotBetween(String value1, String value2) {
            addCriterion("ROOM_TEL not between", value1, value2, "roomTel");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderIsNull() {
            addCriterion("ROOM_LEADER is null");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderIsNotNull() {
            addCriterion("ROOM_LEADER is not null");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderEqualTo(String value) {
            addCriterion("ROOM_LEADER =", value, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderNotEqualTo(String value) {
            addCriterion("ROOM_LEADER <>", value, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderGreaterThan(String value) {
            addCriterion("ROOM_LEADER >", value, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderGreaterThanOrEqualTo(String value) {
            addCriterion("ROOM_LEADER >=", value, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderLessThan(String value) {
            addCriterion("ROOM_LEADER <", value, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderLessThanOrEqualTo(String value) {
            addCriterion("ROOM_LEADER <=", value, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderLike(String value) {
            addCriterion("ROOM_LEADER like", value, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderNotLike(String value) {
            addCriterion("ROOM_LEADER not like", value, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderIn(List<String> values) {
            addCriterion("ROOM_LEADER in", values, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderNotIn(List<String> values) {
            addCriterion("ROOM_LEADER not in", values, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderBetween(String value1, String value2) {
            addCriterion("ROOM_LEADER between", value1, value2, "roomLeader");
            return (Criteria) this;
        }

        public Criteria andRoomLeaderNotBetween(String value1, String value2) {
            addCriterion("ROOM_LEADER not between", value1, value2, "roomLeader");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}