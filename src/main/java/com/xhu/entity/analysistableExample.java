package com.xhu.entity;

import com.xhu.util.BootPage;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class analysistableExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected BootPage page;

    public analysistableExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(BootPage page) {
        this.page=page;
    }

    public BootPage getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("sex is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("sex is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(String value) {
            addCriterion("sex =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(String value) {
            addCriterion("sex <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(String value) {
            addCriterion("sex >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(String value) {
            addCriterion("sex >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(String value) {
            addCriterion("sex <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(String value) {
            addCriterion("sex <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLike(String value) {
            addCriterion("sex like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotLike(String value) {
            addCriterion("sex not like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<String> values) {
            addCriterion("sex in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<String> values) {
            addCriterion("sex not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(String value1, String value2) {
            addCriterion("sex between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(String value1, String value2) {
            addCriterion("sex not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andAgeIsNull() {
            addCriterion("age is null");
            return (Criteria) this;
        }

        public Criteria andAgeIsNotNull() {
            addCriterion("age is not null");
            return (Criteria) this;
        }

        public Criteria andAgeEqualTo(String value) {
            addCriterion("age =", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotEqualTo(String value) {
            addCriterion("age <>", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThan(String value) {
            addCriterion("age >", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThanOrEqualTo(String value) {
            addCriterion("age >=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThan(String value) {
            addCriterion("age <", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThanOrEqualTo(String value) {
            addCriterion("age <=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLike(String value) {
            addCriterion("age like", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotLike(String value) {
            addCriterion("age not like", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeIn(List<String> values) {
            addCriterion("age in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotIn(List<String> values) {
            addCriterion("age not in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeBetween(String value1, String value2) {
            addCriterion("age between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotBetween(String value1, String value2) {
            addCriterion("age not between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andHeightIsNull() {
            addCriterion("height is null");
            return (Criteria) this;
        }

        public Criteria andHeightIsNotNull() {
            addCriterion("height is not null");
            return (Criteria) this;
        }

        public Criteria andHeightEqualTo(Integer value) {
            addCriterion("height =", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotEqualTo(Integer value) {
            addCriterion("height <>", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightGreaterThan(Integer value) {
            addCriterion("height >", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightGreaterThanOrEqualTo(Integer value) {
            addCriterion("height >=", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightLessThan(Integer value) {
            addCriterion("height <", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightLessThanOrEqualTo(Integer value) {
            addCriterion("height <=", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightIn(List<Integer> values) {
            addCriterion("height in", values, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotIn(List<Integer> values) {
            addCriterion("height not in", values, "height");
            return (Criteria) this;
        }

        public Criteria andHeightBetween(Integer value1, Integer value2) {
            addCriterion("height between", value1, value2, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotBetween(Integer value1, Integer value2) {
            addCriterion("height not between", value1, value2, "height");
            return (Criteria) this;
        }

        public Criteria andWeighIsNull() {
            addCriterion("weigh is null");
            return (Criteria) this;
        }

        public Criteria andWeighIsNotNull() {
            addCriterion("weigh is not null");
            return (Criteria) this;
        }

        public Criteria andWeighEqualTo(Integer value) {
            addCriterion("weigh =", value, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighNotEqualTo(Integer value) {
            addCriterion("weigh <>", value, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighGreaterThan(Integer value) {
            addCriterion("weigh >", value, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighGreaterThanOrEqualTo(Integer value) {
            addCriterion("weigh >=", value, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighLessThan(Integer value) {
            addCriterion("weigh <", value, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighLessThanOrEqualTo(Integer value) {
            addCriterion("weigh <=", value, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighIn(List<Integer> values) {
            addCriterion("weigh in", values, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighNotIn(List<Integer> values) {
            addCriterion("weigh not in", values, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighBetween(Integer value1, Integer value2) {
            addCriterion("weigh between", value1, value2, "weigh");
            return (Criteria) this;
        }

        public Criteria andWeighNotBetween(Integer value1, Integer value2) {
            addCriterion("weigh not between", value1, value2, "weigh");
            return (Criteria) this;
        }

        public Criteria andChecktimeIsNull() {
            addCriterion("checktime is null");
            return (Criteria) this;
        }

        public Criteria andChecktimeIsNotNull() {
            addCriterion("checktime is not null");
            return (Criteria) this;
        }

        public Criteria andChecktimeEqualTo(Date value) {
            addCriterionForJDBCDate("checktime =", value, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("checktime <>", value, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeGreaterThan(Date value) {
            addCriterionForJDBCDate("checktime >", value, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("checktime >=", value, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeLessThan(Date value) {
            addCriterionForJDBCDate("checktime <", value, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("checktime <=", value, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeIn(List<Date> values) {
            addCriterionForJDBCDate("checktime in", values, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("checktime not in", values, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("checktime between", value1, value2, "checktime");
            return (Criteria) this;
        }

        public Criteria andChecktimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("checktime not between", value1, value2, "checktime");
            return (Criteria) this;
        }

        public Criteria andIlnameIsNull() {
            addCriterion("ilname is null");
            return (Criteria) this;
        }

        public Criteria andIlnameIsNotNull() {
            addCriterion("ilname is not null");
            return (Criteria) this;
        }

        public Criteria andIlnameEqualTo(String value) {
            addCriterion("ilname =", value, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameNotEqualTo(String value) {
            addCriterion("ilname <>", value, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameGreaterThan(String value) {
            addCriterion("ilname >", value, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameGreaterThanOrEqualTo(String value) {
            addCriterion("ilname >=", value, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameLessThan(String value) {
            addCriterion("ilname <", value, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameLessThanOrEqualTo(String value) {
            addCriterion("ilname <=", value, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameLike(String value) {
            addCriterion("ilname like", value, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameNotLike(String value) {
            addCriterion("ilname not like", value, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameIn(List<String> values) {
            addCriterion("ilname in", values, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameNotIn(List<String> values) {
            addCriterion("ilname not in", values, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameBetween(String value1, String value2) {
            addCriterion("ilname between", value1, value2, "ilname");
            return (Criteria) this;
        }

        public Criteria andIlnameNotBetween(String value1, String value2) {
            addCriterion("ilname not between", value1, value2, "ilname");
            return (Criteria) this;
        }

        public Criteria andDieIsNull() {
            addCriterion("die is null");
            return (Criteria) this;
        }

        public Criteria andDieIsNotNull() {
            addCriterion("die is not null");
            return (Criteria) this;
        }

        public Criteria andDieEqualTo(Integer value) {
            addCriterion("die =", value, "die");
            return (Criteria) this;
        }

        public Criteria andDieNotEqualTo(Integer value) {
            addCriterion("die <>", value, "die");
            return (Criteria) this;
        }

        public Criteria andDieGreaterThan(Integer value) {
            addCriterion("die >", value, "die");
            return (Criteria) this;
        }

        public Criteria andDieGreaterThanOrEqualTo(Integer value) {
            addCriterion("die >=", value, "die");
            return (Criteria) this;
        }

        public Criteria andDieLessThan(Integer value) {
            addCriterion("die <", value, "die");
            return (Criteria) this;
        }

        public Criteria andDieLessThanOrEqualTo(Integer value) {
            addCriterion("die <=", value, "die");
            return (Criteria) this;
        }

        public Criteria andDieIn(List<Integer> values) {
            addCriterion("die in", values, "die");
            return (Criteria) this;
        }

        public Criteria andDieNotIn(List<Integer> values) {
            addCriterion("die not in", values, "die");
            return (Criteria) this;
        }

        public Criteria andDieBetween(Integer value1, Integer value2) {
            addCriterion("die between", value1, value2, "die");
            return (Criteria) this;
        }

        public Criteria andDieNotBetween(Integer value1, Integer value2) {
            addCriterion("die not between", value1, value2, "die");
            return (Criteria) this;
        }

        public Criteria andFastIsNull() {
            addCriterion("fast is null");
            return (Criteria) this;
        }

        public Criteria andFastIsNotNull() {
            addCriterion("fast is not null");
            return (Criteria) this;
        }

        public Criteria andFastEqualTo(Integer value) {
            addCriterion("fast =", value, "fast");
            return (Criteria) this;
        }

        public Criteria andFastNotEqualTo(Integer value) {
            addCriterion("fast <>", value, "fast");
            return (Criteria) this;
        }

        public Criteria andFastGreaterThan(Integer value) {
            addCriterion("fast >", value, "fast");
            return (Criteria) this;
        }

        public Criteria andFastGreaterThanOrEqualTo(Integer value) {
            addCriterion("fast >=", value, "fast");
            return (Criteria) this;
        }

        public Criteria andFastLessThan(Integer value) {
            addCriterion("fast <", value, "fast");
            return (Criteria) this;
        }

        public Criteria andFastLessThanOrEqualTo(Integer value) {
            addCriterion("fast <=", value, "fast");
            return (Criteria) this;
        }

        public Criteria andFastIn(List<Integer> values) {
            addCriterion("fast in", values, "fast");
            return (Criteria) this;
        }

        public Criteria andFastNotIn(List<Integer> values) {
            addCriterion("fast not in", values, "fast");
            return (Criteria) this;
        }

        public Criteria andFastBetween(Integer value1, Integer value2) {
            addCriterion("fast between", value1, value2, "fast");
            return (Criteria) this;
        }

        public Criteria andFastNotBetween(Integer value1, Integer value2) {
            addCriterion("fast not between", value1, value2, "fast");
            return (Criteria) this;
        }

        public Criteria andContagionIsNull() {
            addCriterion("contagion is null");
            return (Criteria) this;
        }

        public Criteria andContagionIsNotNull() {
            addCriterion("contagion is not null");
            return (Criteria) this;
        }

        public Criteria andContagionEqualTo(String value) {
            addCriterion("contagion =", value, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionNotEqualTo(String value) {
            addCriterion("contagion <>", value, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionGreaterThan(String value) {
            addCriterion("contagion >", value, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionGreaterThanOrEqualTo(String value) {
            addCriterion("contagion >=", value, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionLessThan(String value) {
            addCriterion("contagion <", value, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionLessThanOrEqualTo(String value) {
            addCriterion("contagion <=", value, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionLike(String value) {
            addCriterion("contagion like", value, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionNotLike(String value) {
            addCriterion("contagion not like", value, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionIn(List<String> values) {
            addCriterion("contagion in", values, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionNotIn(List<String> values) {
            addCriterion("contagion not in", values, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionBetween(String value1, String value2) {
            addCriterion("contagion between", value1, value2, "contagion");
            return (Criteria) this;
        }

        public Criteria andContagionNotBetween(String value1, String value2) {
            addCriterion("contagion not between", value1, value2, "contagion");
            return (Criteria) this;
        }

        public Criteria andProfessionIsNull() {
            addCriterion("profession is null");
            return (Criteria) this;
        }

        public Criteria andProfessionIsNotNull() {
            addCriterion("profession is not null");
            return (Criteria) this;
        }

        public Criteria andProfessionEqualTo(String value) {
            addCriterion("profession =", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionNotEqualTo(String value) {
            addCriterion("profession <>", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionGreaterThan(String value) {
            addCriterion("profession >", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionGreaterThanOrEqualTo(String value) {
            addCriterion("profession >=", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionLessThan(String value) {
            addCriterion("profession <", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionLessThanOrEqualTo(String value) {
            addCriterion("profession <=", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionLike(String value) {
            addCriterion("profession like", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionNotLike(String value) {
            addCriterion("profession not like", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionIn(List<String> values) {
            addCriterion("profession in", values, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionNotIn(List<String> values) {
            addCriterion("profession not in", values, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionBetween(String value1, String value2) {
            addCriterion("profession between", value1, value2, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionNotBetween(String value1, String value2) {
            addCriterion("profession not between", value1, value2, "profession");
            return (Criteria) this;
        }

        public Criteria andNationIsNull() {
            addCriterion("nation is null");
            return (Criteria) this;
        }

        public Criteria andNationIsNotNull() {
            addCriterion("nation is not null");
            return (Criteria) this;
        }

        public Criteria andNationEqualTo(String value) {
            addCriterion("nation =", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationNotEqualTo(String value) {
            addCriterion("nation <>", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationGreaterThan(String value) {
            addCriterion("nation >", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationGreaterThanOrEqualTo(String value) {
            addCriterion("nation >=", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationLessThan(String value) {
            addCriterion("nation <", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationLessThanOrEqualTo(String value) {
            addCriterion("nation <=", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationLike(String value) {
            addCriterion("nation like", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationNotLike(String value) {
            addCriterion("nation not like", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationIn(List<String> values) {
            addCriterion("nation in", values, "nation");
            return (Criteria) this;
        }

        public Criteria andNationNotIn(List<String> values) {
            addCriterion("nation not in", values, "nation");
            return (Criteria) this;
        }

        public Criteria andNationBetween(String value1, String value2) {
            addCriterion("nation between", value1, value2, "nation");
            return (Criteria) this;
        }

        public Criteria andNationNotBetween(String value1, String value2) {
            addCriterion("nation not between", value1, value2, "nation");
            return (Criteria) this;
        }

        public Criteria andMarriageIsNull() {
            addCriterion("marriage is null");
            return (Criteria) this;
        }

        public Criteria andMarriageIsNotNull() {
            addCriterion("marriage is not null");
            return (Criteria) this;
        }

        public Criteria andMarriageEqualTo(String value) {
            addCriterion("marriage =", value, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageNotEqualTo(String value) {
            addCriterion("marriage <>", value, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageGreaterThan(String value) {
            addCriterion("marriage >", value, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageGreaterThanOrEqualTo(String value) {
            addCriterion("marriage >=", value, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageLessThan(String value) {
            addCriterion("marriage <", value, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageLessThanOrEqualTo(String value) {
            addCriterion("marriage <=", value, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageLike(String value) {
            addCriterion("marriage like", value, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageNotLike(String value) {
            addCriterion("marriage not like", value, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageIn(List<String> values) {
            addCriterion("marriage in", values, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageNotIn(List<String> values) {
            addCriterion("marriage not in", values, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageBetween(String value1, String value2) {
            addCriterion("marriage between", value1, value2, "marriage");
            return (Criteria) this;
        }

        public Criteria andMarriageNotBetween(String value1, String value2) {
            addCriterion("marriage not between", value1, value2, "marriage");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}