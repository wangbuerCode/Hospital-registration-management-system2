package com.xhu.entity;

import com.xhu.util.BootPage;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class medicineExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected BootPage page;

    public medicineExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(BootPage page) {
        this.page=page;
    }

    public BootPage getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMnoidIsNull() {
            addCriterion("MnoID is null");
            return (Criteria) this;
        }

        public Criteria andMnoidIsNotNull() {
            addCriterion("MnoID is not null");
            return (Criteria) this;
        }

        public Criteria andMnoidEqualTo(String value) {
            addCriterion("MnoID =", value, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidNotEqualTo(String value) {
            addCriterion("MnoID <>", value, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidGreaterThan(String value) {
            addCriterion("MnoID >", value, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidGreaterThanOrEqualTo(String value) {
            addCriterion("MnoID >=", value, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidLessThan(String value) {
            addCriterion("MnoID <", value, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidLessThanOrEqualTo(String value) {
            addCriterion("MnoID <=", value, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidLike(String value) {
            addCriterion("MnoID like", value, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidNotLike(String value) {
            addCriterion("MnoID not like", value, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidIn(List<String> values) {
            addCriterion("MnoID in", values, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidNotIn(List<String> values) {
            addCriterion("MnoID not in", values, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidBetween(String value1, String value2) {
            addCriterion("MnoID between", value1, value2, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnoidNotBetween(String value1, String value2) {
            addCriterion("MnoID not between", value1, value2, "mnoid");
            return (Criteria) this;
        }

        public Criteria andMnameIsNull() {
            addCriterion("Mname is null");
            return (Criteria) this;
        }

        public Criteria andMnameIsNotNull() {
            addCriterion("Mname is not null");
            return (Criteria) this;
        }

        public Criteria andMnameEqualTo(String value) {
            addCriterion("Mname =", value, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameNotEqualTo(String value) {
            addCriterion("Mname <>", value, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameGreaterThan(String value) {
            addCriterion("Mname >", value, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameGreaterThanOrEqualTo(String value) {
            addCriterion("Mname >=", value, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameLessThan(String value) {
            addCriterion("Mname <", value, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameLessThanOrEqualTo(String value) {
            addCriterion("Mname <=", value, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameLike(String value) {
            addCriterion("Mname like", value, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameNotLike(String value) {
            addCriterion("Mname not like", value, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameIn(List<String> values) {
            addCriterion("Mname in", values, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameNotIn(List<String> values) {
            addCriterion("Mname not in", values, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameBetween(String value1, String value2) {
            addCriterion("Mname between", value1, value2, "mname");
            return (Criteria) this;
        }

        public Criteria andMnameNotBetween(String value1, String value2) {
            addCriterion("Mname not between", value1, value2, "mname");
            return (Criteria) this;
        }

        public Criteria andMspecsIsNull() {
            addCriterion("Mspecs is null");
            return (Criteria) this;
        }

        public Criteria andMspecsIsNotNull() {
            addCriterion("Mspecs is not null");
            return (Criteria) this;
        }

        public Criteria andMspecsEqualTo(String value) {
            addCriterion("Mspecs =", value, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsNotEqualTo(String value) {
            addCriterion("Mspecs <>", value, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsGreaterThan(String value) {
            addCriterion("Mspecs >", value, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsGreaterThanOrEqualTo(String value) {
            addCriterion("Mspecs >=", value, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsLessThan(String value) {
            addCriterion("Mspecs <", value, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsLessThanOrEqualTo(String value) {
            addCriterion("Mspecs <=", value, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsLike(String value) {
            addCriterion("Mspecs like", value, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsNotLike(String value) {
            addCriterion("Mspecs not like", value, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsIn(List<String> values) {
            addCriterion("Mspecs in", values, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsNotIn(List<String> values) {
            addCriterion("Mspecs not in", values, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsBetween(String value1, String value2) {
            addCriterion("Mspecs between", value1, value2, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMspecsNotBetween(String value1, String value2) {
            addCriterion("Mspecs not between", value1, value2, "mspecs");
            return (Criteria) this;
        }

        public Criteria andMunitIsNull() {
            addCriterion("Munit is null");
            return (Criteria) this;
        }

        public Criteria andMunitIsNotNull() {
            addCriterion("Munit is not null");
            return (Criteria) this;
        }

        public Criteria andMunitEqualTo(String value) {
            addCriterion("Munit =", value, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitNotEqualTo(String value) {
            addCriterion("Munit <>", value, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitGreaterThan(String value) {
            addCriterion("Munit >", value, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitGreaterThanOrEqualTo(String value) {
            addCriterion("Munit >=", value, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitLessThan(String value) {
            addCriterion("Munit <", value, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitLessThanOrEqualTo(String value) {
            addCriterion("Munit <=", value, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitLike(String value) {
            addCriterion("Munit like", value, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitNotLike(String value) {
            addCriterion("Munit not like", value, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitIn(List<String> values) {
            addCriterion("Munit in", values, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitNotIn(List<String> values) {
            addCriterion("Munit not in", values, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitBetween(String value1, String value2) {
            addCriterion("Munit between", value1, value2, "munit");
            return (Criteria) this;
        }

        public Criteria andMunitNotBetween(String value1, String value2) {
            addCriterion("Munit not between", value1, value2, "munit");
            return (Criteria) this;
        }

        public Criteria andMpriceIsNull() {
            addCriterion("Mprice is null");
            return (Criteria) this;
        }

        public Criteria andMpriceIsNotNull() {
            addCriterion("Mprice is not null");
            return (Criteria) this;
        }

        public Criteria andMpriceEqualTo(Integer value) {
            addCriterion("Mprice =", value, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceNotEqualTo(Integer value) {
            addCriterion("Mprice <>", value, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceGreaterThan(Integer value) {
            addCriterion("Mprice >", value, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("Mprice >=", value, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceLessThan(Integer value) {
            addCriterion("Mprice <", value, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceLessThanOrEqualTo(Integer value) {
            addCriterion("Mprice <=", value, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceIn(List<Integer> values) {
            addCriterion("Mprice in", values, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceNotIn(List<Integer> values) {
            addCriterion("Mprice not in", values, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceBetween(Integer value1, Integer value2) {
            addCriterion("Mprice between", value1, value2, "mprice");
            return (Criteria) this;
        }

        public Criteria andMpriceNotBetween(Integer value1, Integer value2) {
            addCriterion("Mprice not between", value1, value2, "mprice");
            return (Criteria) this;
        }

        public Criteria andMdateIsNull() {
            addCriterion("Mdate is null");
            return (Criteria) this;
        }

        public Criteria andMdateIsNotNull() {
            addCriterion("Mdate is not null");
            return (Criteria) this;
        }

        public Criteria andMdateEqualTo(Date value) {
            addCriterionForJDBCDate("Mdate =", value, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateNotEqualTo(Date value) {
            addCriterionForJDBCDate("Mdate <>", value, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateGreaterThan(Date value) {
            addCriterionForJDBCDate("Mdate >", value, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Mdate >=", value, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateLessThan(Date value) {
            addCriterionForJDBCDate("Mdate <", value, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Mdate <=", value, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateIn(List<Date> values) {
            addCriterionForJDBCDate("Mdate in", values, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateNotIn(List<Date> values) {
            addCriterionForJDBCDate("Mdate not in", values, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Mdate between", value1, value2, "mdate");
            return (Criteria) this;
        }

        public Criteria andMdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Mdate not between", value1, value2, "mdate");
            return (Criteria) this;
        }

        public Criteria andMvalIsNull() {
            addCriterion("Mval is null");
            return (Criteria) this;
        }

        public Criteria andMvalIsNotNull() {
            addCriterion("Mval is not null");
            return (Criteria) this;
        }

        public Criteria andMvalEqualTo(Integer value) {
            addCriterion("Mval =", value, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalNotEqualTo(Integer value) {
            addCriterion("Mval <>", value, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalGreaterThan(Integer value) {
            addCriterion("Mval >", value, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalGreaterThanOrEqualTo(Integer value) {
            addCriterion("Mval >=", value, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalLessThan(Integer value) {
            addCriterion("Mval <", value, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalLessThanOrEqualTo(Integer value) {
            addCriterion("Mval <=", value, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalIn(List<Integer> values) {
            addCriterion("Mval in", values, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalNotIn(List<Integer> values) {
            addCriterion("Mval not in", values, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalBetween(Integer value1, Integer value2) {
            addCriterion("Mval between", value1, value2, "mval");
            return (Criteria) this;
        }

        public Criteria andMvalNotBetween(Integer value1, Integer value2) {
            addCriterion("Mval not between", value1, value2, "mval");
            return (Criteria) this;
        }

        public Criteria andMnumberIsNull() {
            addCriterion("Mnumber is null");
            return (Criteria) this;
        }

        public Criteria andMnumberIsNotNull() {
            addCriterion("Mnumber is not null");
            return (Criteria) this;
        }

        public Criteria andMnumberEqualTo(Integer value) {
            addCriterion("Mnumber =", value, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberNotEqualTo(Integer value) {
            addCriterion("Mnumber <>", value, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberGreaterThan(Integer value) {
            addCriterion("Mnumber >", value, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("Mnumber >=", value, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberLessThan(Integer value) {
            addCriterion("Mnumber <", value, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberLessThanOrEqualTo(Integer value) {
            addCriterion("Mnumber <=", value, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberIn(List<Integer> values) {
            addCriterion("Mnumber in", values, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberNotIn(List<Integer> values) {
            addCriterion("Mnumber not in", values, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberBetween(Integer value1, Integer value2) {
            addCriterion("Mnumber between", value1, value2, "mnumber");
            return (Criteria) this;
        }

        public Criteria andMnumberNotBetween(Integer value1, Integer value2) {
            addCriterion("Mnumber not between", value1, value2, "mnumber");
            return (Criteria) this;
        }

        public Criteria andSnoIsNull() {
            addCriterion("Sno is null");
            return (Criteria) this;
        }

        public Criteria andSnoIsNotNull() {
            addCriterion("Sno is not null");
            return (Criteria) this;
        }

        public Criteria andSnoEqualTo(String value) {
            addCriterion("Sno =", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotEqualTo(String value) {
            addCriterion("Sno <>", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoGreaterThan(String value) {
            addCriterion("Sno >", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoGreaterThanOrEqualTo(String value) {
            addCriterion("Sno >=", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLessThan(String value) {
            addCriterion("Sno <", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLessThanOrEqualTo(String value) {
            addCriterion("Sno <=", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLike(String value) {
            addCriterion("Sno like", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotLike(String value) {
            addCriterion("Sno not like", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoIn(List<String> values) {
            addCriterion("Sno in", values, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotIn(List<String> values) {
            addCriterion("Sno not in", values, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoBetween(String value1, String value2) {
            addCriterion("Sno between", value1, value2, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotBetween(String value1, String value2) {
            addCriterion("Sno not between", value1, value2, "sno");
            return (Criteria) this;
        }

        public Criteria andIscfIsNull() {
            addCriterion("isCF is null");
            return (Criteria) this;
        }

        public Criteria andIscfIsNotNull() {
            addCriterion("isCF is not null");
            return (Criteria) this;
        }

        public Criteria andIscfEqualTo(String value) {
            addCriterion("isCF =", value, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfNotEqualTo(String value) {
            addCriterion("isCF <>", value, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfGreaterThan(String value) {
            addCriterion("isCF >", value, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfGreaterThanOrEqualTo(String value) {
            addCriterion("isCF >=", value, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfLessThan(String value) {
            addCriterion("isCF <", value, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfLessThanOrEqualTo(String value) {
            addCriterion("isCF <=", value, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfLike(String value) {
            addCriterion("isCF like", value, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfNotLike(String value) {
            addCriterion("isCF not like", value, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfIn(List<String> values) {
            addCriterion("isCF in", values, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfNotIn(List<String> values) {
            addCriterion("isCF not in", values, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfBetween(String value1, String value2) {
            addCriterion("isCF between", value1, value2, "iscf");
            return (Criteria) this;
        }

        public Criteria andIscfNotBetween(String value1, String value2) {
            addCriterion("isCF not between", value1, value2, "iscf");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}