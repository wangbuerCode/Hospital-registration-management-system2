package com.xhu.entity;

import com.xhu.util.BootPage;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ifodoctorExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected BootPage page;

    public ifodoctorExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(BootPage page) {
        this.page=page;
    }

    public BootPage getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDoctorIdIsNull() {
            addCriterion("DOCTOR_ID is null");
            return (Criteria) this;
        }

        public Criteria andDoctorIdIsNotNull() {
            addCriterion("DOCTOR_ID is not null");
            return (Criteria) this;
        }

        public Criteria andDoctorIdEqualTo(String value) {
            addCriterion("DOCTOR_ID =", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdNotEqualTo(String value) {
            addCriterion("DOCTOR_ID <>", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdGreaterThan(String value) {
            addCriterion("DOCTOR_ID >", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdGreaterThanOrEqualTo(String value) {
            addCriterion("DOCTOR_ID >=", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdLessThan(String value) {
            addCriterion("DOCTOR_ID <", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdLessThanOrEqualTo(String value) {
            addCriterion("DOCTOR_ID <=", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdLike(String value) {
            addCriterion("DOCTOR_ID like", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdNotLike(String value) {
            addCriterion("DOCTOR_ID not like", value, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdIn(List<String> values) {
            addCriterion("DOCTOR_ID in", values, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdNotIn(List<String> values) {
            addCriterion("DOCTOR_ID not in", values, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdBetween(String value1, String value2) {
            addCriterion("DOCTOR_ID between", value1, value2, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorIdNotBetween(String value1, String value2) {
            addCriterion("DOCTOR_ID not between", value1, value2, "doctorId");
            return (Criteria) this;
        }

        public Criteria andDoctorNameIsNull() {
            addCriterion("DOCTOR_NAME is null");
            return (Criteria) this;
        }

        public Criteria andDoctorNameIsNotNull() {
            addCriterion("DOCTOR_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andDoctorNameEqualTo(String value) {
            addCriterion("DOCTOR_NAME =", value, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameNotEqualTo(String value) {
            addCriterion("DOCTOR_NAME <>", value, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameGreaterThan(String value) {
            addCriterion("DOCTOR_NAME >", value, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameGreaterThanOrEqualTo(String value) {
            addCriterion("DOCTOR_NAME >=", value, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameLessThan(String value) {
            addCriterion("DOCTOR_NAME <", value, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameLessThanOrEqualTo(String value) {
            addCriterion("DOCTOR_NAME <=", value, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameLike(String value) {
            addCriterion("DOCTOR_NAME like", value, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameNotLike(String value) {
            addCriterion("DOCTOR_NAME not like", value, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameIn(List<String> values) {
            addCriterion("DOCTOR_NAME in", values, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameNotIn(List<String> values) {
            addCriterion("DOCTOR_NAME not in", values, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameBetween(String value1, String value2) {
            addCriterion("DOCTOR_NAME between", value1, value2, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorNameNotBetween(String value1, String value2) {
            addCriterion("DOCTOR_NAME not between", value1, value2, "doctorName");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdIsNull() {
            addCriterion("DOCTOR_PRO_ID is null");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdIsNotNull() {
            addCriterion("DOCTOR_PRO_ID is not null");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdEqualTo(String value) {
            addCriterion("DOCTOR_PRO_ID =", value, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdNotEqualTo(String value) {
            addCriterion("DOCTOR_PRO_ID <>", value, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdGreaterThan(String value) {
            addCriterion("DOCTOR_PRO_ID >", value, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdGreaterThanOrEqualTo(String value) {
            addCriterion("DOCTOR_PRO_ID >=", value, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdLessThan(String value) {
            addCriterion("DOCTOR_PRO_ID <", value, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdLessThanOrEqualTo(String value) {
            addCriterion("DOCTOR_PRO_ID <=", value, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdLike(String value) {
            addCriterion("DOCTOR_PRO_ID like", value, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdNotLike(String value) {
            addCriterion("DOCTOR_PRO_ID not like", value, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdIn(List<String> values) {
            addCriterion("DOCTOR_PRO_ID in", values, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdNotIn(List<String> values) {
            addCriterion("DOCTOR_PRO_ID not in", values, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdBetween(String value1, String value2) {
            addCriterion("DOCTOR_PRO_ID between", value1, value2, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorProIdNotBetween(String value1, String value2) {
            addCriterion("DOCTOR_PRO_ID not between", value1, value2, "doctorProId");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeIsNull() {
            addCriterion("DOCTOR_ENTRY_TIME is null");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeIsNotNull() {
            addCriterion("DOCTOR_ENTRY_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeEqualTo(Date value) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME =", value, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME <>", value, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME >", value, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME >=", value, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeLessThan(Date value) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME <", value, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME <=", value, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeIn(List<Date> values) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME in", values, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME not in", values, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME between", value1, value2, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andDoctorEntryTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("DOCTOR_ENTRY_TIME not between", value1, value2, "doctorEntryTime");
            return (Criteria) this;
        }

        public Criteria andExitStatuIsNull() {
            addCriterion("EXIT_STATU is null");
            return (Criteria) this;
        }

        public Criteria andExitStatuIsNotNull() {
            addCriterion("EXIT_STATU is not null");
            return (Criteria) this;
        }

        public Criteria andExitStatuEqualTo(String value) {
            addCriterion("EXIT_STATU =", value, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuNotEqualTo(String value) {
            addCriterion("EXIT_STATU <>", value, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuGreaterThan(String value) {
            addCriterion("EXIT_STATU >", value, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuGreaterThanOrEqualTo(String value) {
            addCriterion("EXIT_STATU >=", value, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuLessThan(String value) {
            addCriterion("EXIT_STATU <", value, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuLessThanOrEqualTo(String value) {
            addCriterion("EXIT_STATU <=", value, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuLike(String value) {
            addCriterion("EXIT_STATU like", value, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuNotLike(String value) {
            addCriterion("EXIT_STATU not like", value, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuIn(List<String> values) {
            addCriterion("EXIT_STATU in", values, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuNotIn(List<String> values) {
            addCriterion("EXIT_STATU not in", values, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuBetween(String value1, String value2) {
            addCriterion("EXIT_STATU between", value1, value2, "exitStatu");
            return (Criteria) this;
        }

        public Criteria andExitStatuNotBetween(String value1, String value2) {
            addCriterion("EXIT_STATU not between", value1, value2, "exitStatu");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}