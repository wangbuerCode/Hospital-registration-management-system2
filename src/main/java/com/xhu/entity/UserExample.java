package com.xhu.entity;

import com.xhu.util.BootPage;
import java.util.ArrayList;
import java.util.List;

public class UserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected BootPage page;

    public UserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(BootPage page) {
        this.page=page;
    }

    public BootPage getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserIdIsNull() {
            addCriterion("USER_ID is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("USER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("USER_ID =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("USER_ID <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("USER_ID >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("USER_ID >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("USER_ID <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("USER_ID <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("USER_ID in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("USER_ID not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("USER_ID between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("USER_ID not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("USER_NAME is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("USER_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("USER_NAME =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("USER_NAME <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("USER_NAME >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("USER_NAME >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("USER_NAME <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("USER_NAME <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("USER_NAME like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("USER_NAME not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("USER_NAME in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("USER_NAME not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("USER_NAME between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("USER_NAME not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIsNull() {
            addCriterion("USER_PASSWORD is null");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIsNotNull() {
            addCriterion("USER_PASSWORD is not null");
            return (Criteria) this;
        }

        public Criteria andUserPasswordEqualTo(String value) {
            addCriterion("USER_PASSWORD =", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotEqualTo(String value) {
            addCriterion("USER_PASSWORD <>", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordGreaterThan(String value) {
            addCriterion("USER_PASSWORD >", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("USER_PASSWORD >=", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLessThan(String value) {
            addCriterion("USER_PASSWORD <", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLessThanOrEqualTo(String value) {
            addCriterion("USER_PASSWORD <=", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordLike(String value) {
            addCriterion("USER_PASSWORD like", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotLike(String value) {
            addCriterion("USER_PASSWORD not like", value, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordIn(List<String> values) {
            addCriterion("USER_PASSWORD in", values, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotIn(List<String> values) {
            addCriterion("USER_PASSWORD not in", values, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordBetween(String value1, String value2) {
            addCriterion("USER_PASSWORD between", value1, value2, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserPasswordNotBetween(String value1, String value2) {
            addCriterion("USER_PASSWORD not between", value1, value2, "userPassword");
            return (Criteria) this;
        }

        public Criteria andUserTelIsNull() {
            addCriterion("USER_TEL is null");
            return (Criteria) this;
        }

        public Criteria andUserTelIsNotNull() {
            addCriterion("USER_TEL is not null");
            return (Criteria) this;
        }

        public Criteria andUserTelEqualTo(String value) {
            addCriterion("USER_TEL =", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelNotEqualTo(String value) {
            addCriterion("USER_TEL <>", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelGreaterThan(String value) {
            addCriterion("USER_TEL >", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelGreaterThanOrEqualTo(String value) {
            addCriterion("USER_TEL >=", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelLessThan(String value) {
            addCriterion("USER_TEL <", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelLessThanOrEqualTo(String value) {
            addCriterion("USER_TEL <=", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelLike(String value) {
            addCriterion("USER_TEL like", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelNotLike(String value) {
            addCriterion("USER_TEL not like", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelIn(List<String> values) {
            addCriterion("USER_TEL in", values, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelNotIn(List<String> values) {
            addCriterion("USER_TEL not in", values, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelBetween(String value1, String value2) {
            addCriterion("USER_TEL between", value1, value2, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelNotBetween(String value1, String value2) {
            addCriterion("USER_TEL not between", value1, value2, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserPidIsNull() {
            addCriterion("USER_PID is null");
            return (Criteria) this;
        }

        public Criteria andUserPidIsNotNull() {
            addCriterion("USER_PID is not null");
            return (Criteria) this;
        }

        public Criteria andUserPidEqualTo(String value) {
            addCriterion("USER_PID =", value, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidNotEqualTo(String value) {
            addCriterion("USER_PID <>", value, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidGreaterThan(String value) {
            addCriterion("USER_PID >", value, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidGreaterThanOrEqualTo(String value) {
            addCriterion("USER_PID >=", value, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidLessThan(String value) {
            addCriterion("USER_PID <", value, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidLessThanOrEqualTo(String value) {
            addCriterion("USER_PID <=", value, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidLike(String value) {
            addCriterion("USER_PID like", value, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidNotLike(String value) {
            addCriterion("USER_PID not like", value, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidIn(List<String> values) {
            addCriterion("USER_PID in", values, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidNotIn(List<String> values) {
            addCriterion("USER_PID not in", values, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidBetween(String value1, String value2) {
            addCriterion("USER_PID between", value1, value2, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserPidNotBetween(String value1, String value2) {
            addCriterion("USER_PID not between", value1, value2, "userPid");
            return (Criteria) this;
        }

        public Criteria andUserRoomIsNull() {
            addCriterion("USER_ROOM is null");
            return (Criteria) this;
        }

        public Criteria andUserRoomIsNotNull() {
            addCriterion("USER_ROOM is not null");
            return (Criteria) this;
        }

        public Criteria andUserRoomEqualTo(String value) {
            addCriterion("USER_ROOM =", value, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomNotEqualTo(String value) {
            addCriterion("USER_ROOM <>", value, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomGreaterThan(String value) {
            addCriterion("USER_ROOM >", value, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomGreaterThanOrEqualTo(String value) {
            addCriterion("USER_ROOM >=", value, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomLessThan(String value) {
            addCriterion("USER_ROOM <", value, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomLessThanOrEqualTo(String value) {
            addCriterion("USER_ROOM <=", value, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomLike(String value) {
            addCriterion("USER_ROOM like", value, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomNotLike(String value) {
            addCriterion("USER_ROOM not like", value, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomIn(List<String> values) {
            addCriterion("USER_ROOM in", values, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomNotIn(List<String> values) {
            addCriterion("USER_ROOM not in", values, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomBetween(String value1, String value2) {
            addCriterion("USER_ROOM between", value1, value2, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserRoomNotBetween(String value1, String value2) {
            addCriterion("USER_ROOM not between", value1, value2, "userRoom");
            return (Criteria) this;
        }

        public Criteria andUserProIsNull() {
            addCriterion("USER_PRO is null");
            return (Criteria) this;
        }

        public Criteria andUserProIsNotNull() {
            addCriterion("USER_PRO is not null");
            return (Criteria) this;
        }

        public Criteria andUserProEqualTo(String value) {
            addCriterion("USER_PRO =", value, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProNotEqualTo(String value) {
            addCriterion("USER_PRO <>", value, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProGreaterThan(String value) {
            addCriterion("USER_PRO >", value, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProGreaterThanOrEqualTo(String value) {
            addCriterion("USER_PRO >=", value, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProLessThan(String value) {
            addCriterion("USER_PRO <", value, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProLessThanOrEqualTo(String value) {
            addCriterion("USER_PRO <=", value, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProLike(String value) {
            addCriterion("USER_PRO like", value, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProNotLike(String value) {
            addCriterion("USER_PRO not like", value, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProIn(List<String> values) {
            addCriterion("USER_PRO in", values, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProNotIn(List<String> values) {
            addCriterion("USER_PRO not in", values, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProBetween(String value1, String value2) {
            addCriterion("USER_PRO between", value1, value2, "userPro");
            return (Criteria) this;
        }

        public Criteria andUserProNotBetween(String value1, String value2) {
            addCriterion("USER_PRO not between", value1, value2, "userPro");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}