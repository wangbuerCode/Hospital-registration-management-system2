package com.xhu.entity;

import java.io.Serializable;
import java.util.Date;

public class checklist implements Serializable {
    private Integer checkId;

    private String doctorId;

    private String name;

    private String sex;

    private Integer age;

    private String checkroom;

    private Date checktime;

    private String checkKind;

    private String checkResult;

    private static final long serialVersionUID = 1L;

    public Integer getCheckId() {
        return checkId;
    }

    public void setCheckId(Integer checkId) {
        this.checkId = checkId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCheckroom() {
        return checkroom;
    }

    public void setCheckroom(String checkroom) {
        this.checkroom = checkroom;
    }

    public Date getChecktime() {
        return checktime;
    }

    public void setChecktime(Date checktime) {
        this.checktime = checktime;
    }

    public String getCheckKind() {
        return checkKind;
    }

    public void setCheckKind(String checkKind) {
        this.checkKind = checkKind;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }
}