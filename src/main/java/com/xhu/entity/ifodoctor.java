package com.xhu.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ifodoctor implements Serializable {
	private Integer id;

	private String doctorId;

	private String doctorName;

	private String doctorProId;

	private Date doctorEntryTime;

	private String exitStatu;
	private List<roomlist> rooms;
		
	public List<roomlist> getRooms() {
		return rooms;
	}

	public void setRooms(List<roomlist> rooms) {
		this.rooms = rooms;
	}

	private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDoctorProId() {
		return doctorProId;
	}

	public void setDoctorProId(String doctorProId) {
		this.doctorProId = doctorProId;
	}

	public Date getDoctorEntryTime() {
		return doctorEntryTime;
	}

	public void setDoctorEntryTime(Date doctorEntryTime) {
		this.doctorEntryTime = doctorEntryTime;
	}

	public String getExitStatu() {
		return exitStatu;
	}

	public void setExitStatu(String exitStatu) {
		this.exitStatu = exitStatu;
	}
}