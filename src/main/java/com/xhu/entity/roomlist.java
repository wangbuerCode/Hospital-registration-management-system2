package com.xhu.entity;

import java.io.Serializable;

public class roomlist implements Serializable {
    private Integer id;

	private String roomId;

	private String roomName;

	private String roomTel;

	private String roomLeader;

	private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomTel() {
		return roomTel;
	}

	public void setRoomTel(String roomTel) {
		this.roomTel = roomTel;
	}

	public String getRoomLeader() {
		return roomLeader;
	}

	public void setRoomLeader(String roomLeader) {
		this.roomLeader = roomLeader;
	}
}