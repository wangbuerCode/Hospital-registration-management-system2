package com.xhu.controller;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.xhu.entity.analysistable;
import com.xhu.entity.medicine;
import com.xhu.entity.medicineMedia;
import com.xhu.entity.medicineRecord;
import com.xhu.entity.treatmentrecord;
import com.xhu.service.DoctorService;
import com.xhu.service.MedicineService;
import com.xhu.util.BaseAction;
import com.xhu.util.BootPage;

@RequestMapping("/Medicine")
@Controller
public class Medicine extends BaseAction {
	@Autowired
	MedicineService MS;
	@Autowired
	DoctorService DC;

	@RequestMapping("/me.html")
	public String Me() {
		return "/jsp/medicine/Medicine";
	}
	/**
	 * 
	 * @Title: informationList 
	 * @Description:
	 * @param page
	 * @param name
	 * @return
	 * @author lb  lb  
	 * @date 2019年6月1日 下午4:53:37 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/pageTest.shtml")
	@ResponseBody
	public BootPage<medicine> informationList(BootPage<medicine> page, @RequestParam("name") String name) {
		return MS.getMedicine(page, name);
	}

	@RequestMapping("/pageTest1.shtml")
	@ResponseBody
	public BootPage<medicineMedia> informationListMa(BootPage<medicineMedia> page) {
		return MS.getMedicineMd(page);
	}

	@RequestMapping("medicine.html")
	public String reMe() {
		return "/jsp/medicine/importmedicine";
	}

	/**
	 * 
	 * @Title: importmedicine
	 * @Description: TODO(导入药品清单)
	 * @param file
	 * @param request
	 * @return
	 * @author lb lb
	 * @date 2018年12月28日 下午4:01:29
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/importfile")
	@ResponseBody
	public ResultObj importmedicine(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		try {
			// System.out.println(file.getOriginalFilename());
			InputStream fileStream = file.getInputStream();
			MS.parseExcel(file.getOriginalFilename(), fileStream);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}

	/**
	 * 
	 * @Title: insert
	 * @Description: TODO(插入诊疗记录)
	 * @return
	 * @author lb lb
	 * @date 2018年12月28日 下午4:01:03
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/insert")
	@ResponseBody
	public ResultObj insert() {
		try {
			MS.insert();
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}

	}

	/**
	 * 
	 * @Title: getorderlist
	 * @Description: TODO(获取药品列表)
	 * @param page
	 * @return
	 * @author lb lb
	 * @date 2018年12月28日 下午4:00:42
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/getorderlist")
	@ResponseBody
	public BootPage<treatmentrecord> getorderlist(BootPage<treatmentrecord> page, String search) {
		return MS.getTre(page, search);

	}

	/**
	 * 
	 * @Title: Confirm
	 * @Description: 确认药品出库的控制器
	 * @return
	 * @author lb lb
	 * @date 2018年12月28日 下午4:00:06
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/confirm.json")
	@ResponseBody
	public ResultObj Confirm(@RequestParam("number") String num, @RequestParam("id") String id) {
		try {
			MS.confirmService(num,id);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}

	/**
	 * 
	 * @Title: dele
	 * @Description: 医生操作删除诊疗记录
	 * @param id
	 * @return
	 * @author lb lb
	 * @date 2018年12月31日 上午11:40:28
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/dele")
	@ResponseBody
	public ResultObj dele(String id) {
		try {
			DC.del(id);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}

	/**
	 * 
	 * @Title: comfirmResult
	 * @Description: 确诊疾病
	 * @return
	 * @author lb lb
	 * @date 2019年2月27日 下午3:07:02
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/comfirmresult")
	@ResponseBody
	public String comfirmResult(analysistable al) {
		if (MS.confirm(al)) {
			return "success";
		} else {
			return "fail";
		}
	}
	/**
	 * 
	 * @Title: informationList 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param page
	 * @param name
	 * @return
	 * @author lb  lb  
	 * @date 2019年3月4日 下午4:08:01 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/medinicerecord")
	@ResponseBody
	public BootPage<medicineRecord> informationList1(BootPage<medicineRecord> page,@RequestParam("name") String name) {
		return MS.getMedicine1(page);
	}
}
