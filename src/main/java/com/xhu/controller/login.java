package com.xhu.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xhu.entity.news;
import com.xhu.service.DelLoginService;
import com.xhu.util.BaseAction;

@RequestMapping("/log")
@Controller
public class login extends BaseAction {
	static final String LOGINURL = "do.html";
	@Autowired
	DelLoginService delLogin;

	@RequestMapping("login.html")
	public String myt() {
		return "/jsp/main/main";
	}

	@RequestMapping("login1.html")
	public String myt1() {
		return "/jsp/main/mainR";
	}

	@RequestMapping("login2.html")
	public String myt2() {
		return "/jsp/main/maindoctor";
	}

	@RequestMapping("login3.html")
	public String myt3() {
		return "/jsp/main/mainuser";
	}

	@RequestMapping("me.html")
	public String Me() {
		return "/jsp/medicine/Medicine";
	}

	@RequestMapping("do.html")
	public String myDo() {
		return "jsp/main/loginPage";
	}

	@RequestMapping("regiest.html")
	public String myRe() {
		return "jsp/main/regiest";
	}

	@RequestMapping("doctor.html")
	public String myDoctor() {
		return "jsp/doctor/doctor";
	}

	@RequestMapping("order.html")
	public String myorder() {
		return "jsp/doctor/orderlist";
	}

	@RequestMapping("lookorder.html")
	public String mylookorder() {
		return "jsp/medicine/lookorderlist";
	}

	/**
	 * 
	 * @Title: doc
	 * @Description: TODO(医生信息界面)
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 上午11:45:02
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("docifo.html")
	public String doc() {
		return "jsp/roomManger/doctorManager";
	}

	/**
	 * 
	 * @Title: usermanger
	 * @Description: 用户管理
	 * @return
	 * @author lb lb
	 * @date 2019年5月26日 下午12:53:24
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/user.html")
	public String usermanger() {
		return "jsp/roomManger/UserManager";
	}

	/**
	 * 
	 * @Title: mylist
	 * @Description: TODO(检查单页面跳转)
	 * @return
	 * @author lb lb
	 * @date 2018年12月31日 下午4:40:18
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("list.html")
	public String mylist() {
		return "jsp/doctor/checklist";
	}

	/**
	 * 
	 * @Title: myneike
	 * @Description: TODO(内科检查跳转)
	 * @return
	 * @author lb lb
	 * @date 2018年12月31日 下午5:14:44
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("neike.html")
	public String myneike() {
		return "jsp/doctor/internal";
	}

	/**
	 * 
	 * @Title: room
	 * @Description: TODO(科室管理)
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 上午11:39:57
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("room.html")
	public String room() {
		return "jsp/roomManger/roomManger";
	}

	/**
	 * 药品锟藉单锟斤拷锟斤拷
	 * 
	 * @return
	 */
	@RequestMapping("medicine.html")
	public String reMe() {
		return "/jsp/medicine/importmedicine";
	}

	/**
	 * 
	 * @Title: loginDo
	 * @Description: 处理登录
	 * @param username
	 * @param password
	 * @param request
	 * @param role
	 * @return
	 * @author lb lb
	 * @date 2019年2月25日 下午3:17:35
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/logindo.json")
	@ResponseBody
	public ResultObj loginDo(@RequestParam("username") String username, @RequestParam("password") String password,
			HttpServletRequest request, String role) {
		try {
			delLogin.delLogin(username, password, request, role);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}

	/**
	 * 
	 * @Title: registDo
	 * @Description: 注册
	 * @param username
	 * @param password
	 * @param idcard
	 * @return
	 * @author lb lb
	 * @date 2019年2月25日 下午3:17:53
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/regist.json")
	@ResponseBody
	public String registDo(@RequestParam("username") String username, @RequestParam("password") String password,
			String idcard) {
		try {
			delLogin.insertId(username, password, idcard);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			return "notex";
		}
	}

	/**
	 * 
	 * @Title: jb
	 * @Description: 疾病预测跳转页面
	 * @return
	 * @author lb lb
	 * @date 2019年1月5日 下午10:13:06
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("jibing")
	public String jb() {
		return "jsp/roomManger/jibingyuce";
	}

	/**
	 * 
	 * @Title: jb
	 * @Description: TODO(男女人数比列)
	 * @return
	 * @author lb lb
	 * @date 2019年1月6日 下午3:22:03
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("sex.html")
	public String jbsex() {
		return "jsp/roomManger/sexnum";
	}

	/**
	 * 
	 * @Title: jbsexage
	 * @Description: TODO(年龄段男女比列)
	 * @return
	 * @author lb lb
	 * @date 2019年1月6日 下午5:30:58
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("sexage.html")
	public String jbsexage() {
		return "jsp/roomManger/sexage";
	}

	/**
	 * 
	 * @Title: circle
	 * @Description: 圆饼统计图
	 * @return
	 * @author lb lb
	 * @date 2019年1月7日 上午10:53:22
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("circle.html")
	public String circle() {
		return "jsp/roomManger/circle";
	}

	/**
	 * 
	 * @Title: more
	 * @Description: TODO(多数据预测)
	 * @return
	 * @author lb lb
	 * @date 2019年1月8日 上午9:20:13
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("moredata.html")
	public String more() {
		return "jsp/roomManger/moredata";
	}

	/**
	 * 
	 * @Title: map
	 * @Description: 地图跳转
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午4:52:00
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("mapdata.html")
	public String map() {
		return "jsp/roomManger/map";
	}

	/**
	 * 
	 * @Title: num
	 * @Description: 疾病的数量和发病率
	 * @return
	 * @author lb lb
	 * @date 2019年1月11日 下午1:26:14
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("numindex.html")
	public String num() {
		return "jsp/roomManger/OneilNumAndIndex";
	}

	/**
	 * 
	 * @Title: publishNews
	 * @Description: 发布心情
	 * @return
	 * @author lb lb
	 * @date 2019年2月18日 下午1:20:25
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/publicnews")
	@ResponseBody
	public String publishNews(@RequestParam("context") String context, HttpServletRequest request) {
		if (delLogin.insertNews(context, request)) {
			return "success";
		} else {
			return "fail";
		}
	}

	/**
	 * 
	 * @Title: listNew
	 * @Description: 获取留言数据
	 * @return
	 * @author lb lb
	 * @date 2019年2月18日 下午3:26:51
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/listNews")
	@ResponseBody
	public List<news> listNew() {
		return delLogin.listNews();
	}

	/**
	 * 
	 * @Title: userlook
	 * @Description: 病人查看挂号纪录跳转
	 * @return
	 * @author lb lb
	 * @date 2019年2月20日 下午1:54:16
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/userlook")
	public String userlook() {
		return "jsp/doctor/userlook";
	}

	/**
	 * 
	 * @Title: page
	 * @Description: 跳转主页
	 * @return
	 * @author lb lb
	 * @date 2019年2月25日 下午3:31:43
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/pageU.html")
	public String page1() {
		return "jsp/roomManger/page";
	}

	/**
	 * 
	 * @Title: page
	 * @Description: 跳转主页
	 * @return
	 * @author lb lb
	 * @date 2019年2月25日 下午3:31:43
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/pageR.html")
	public String page2() {
		return "jsp/roomManger/page";
	}

	/**
	 * 
	 * @Title: page
	 * @Description: 跳转主页
	 * @return
	 * @author lb lb
	 * @date 2019年2月25日 下午3:31:43
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/pageD.html")
	public String page3() {
		return "jsp/roomManger/page";
	}

	/**
	 * 
	 * @Title: page
	 * @Description: 跳转主页
	 * @return
	 * @author lb lb
	 * @date 2019年2月25日 下午3:31:43
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/page.html")
	public String page() {
		return "jsp/roomManger/page";
	}

	/**
	 * 
	 * @Title: loginout
	 * @Description:退出登录
	 * @author lb lb
	 * @date 2019年2月27日 下午1:15:44
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/loginout")
	public void loginout(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.invalidate();
		try {
			response.sendRedirect(LOGINURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: userifo
	 * @Description: 用户填写身份信息
	 * @return
	 * @author lb lb
	 * @date 2019年2月27日 下午2:25:42
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/userifo.html")
	public String userifo() {
		return "jsp/roomManger/userifo";
	}

	/**
	 * 
	 * @Title: TgYear
	 * @Description: 年度报表
	 * @return
	 * @author lb lb
	 * @date 2019年3月10日 下午3:02:06
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/Tgyear")
	public String TgYear() {
		return "jsp/roomManger/Tg_year";
	}

	/**
	 * 
	 * @Title: Tgmouth
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @return
	 * @author lb lb
	 * @date 2019年3月10日 下午3:06:32
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/Tgmouth")
	public String Tgmouth() {
		return "jsp/roomManger/Tg_mouth";
	}
}
