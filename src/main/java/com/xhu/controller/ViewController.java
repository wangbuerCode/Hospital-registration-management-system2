package com.xhu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xhu.util.BaseAction;

@Controller
@RequestMapping("/view")
public class ViewController extends BaseAction {
	/**
	 * 
	 * @Title: viewMain
	 * @Description:浏览主页
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午2:29:42
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/mainview")
	public String viewMain() {
		return "jsp/view/mainview";
	}

	/**
	 * 
	 * @Title: heal
	 * @Description: 健康主题
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午2:30:05
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/he.html")
	public String heal() {
		return "jsp/view/health";
	}

	/**
	 * 
	 * @Title: science
	 * @Description: 科研机构
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午2:30:22
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/science")
	public String science() {
		return "jsp/view/science";
	}

	/**
	 * 
	 * @Title: information
	 * @Description: 公告跳转
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午2:44:50
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/ifo")
	public String information() {
		return "jsp/view/view_ifo";
	}

	/**
	 * 
	 * @Title: tuber
	 * @Description: 结核病跳转
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午2:58:40
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/tuber")
	public String tuber() {
		return "jsp/view/health_topic/tuber";
	}

	/**
	 * 
	 * @Title: tuber
	 * @Description: 艾滋病跳转
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午2:58:40
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/hiv")
	public String hiv() {
		return "jsp/view/health_topic/hiv";
	}

	/**
	 * 
	 * @Title: dog
	 * @Description: 狂犬病跳转
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午3:18:49
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/dog")
	public String dog() {
		return "jsp/view/health_topic/dogil";
	}

	/**
	 * 
	 * @Title: diabetes
	 * @Description: 糖尿病
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午3:55:03
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/diabetes")
	public String diabetes() {
		return "jsp/view/chronic/diabetes";
	}
	/**
	 * 
	 * @Title: diabetes
	 * @Description: 心血管
	 * @return
	 * @author lb 
	 * @date 2019年2月16日 下午3:55:03
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/angio")
	public String diabetea() {
		return "jsp/view/chronic/angiocardiopathy";
	}
	/**
	 * 
	 * @Title: defend 
	 * @Description: 病虫防护跳转
	 * @return
	 * @author lb    
	 * @date 2019年2月16日 下午4:23:04 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/defend")
	public String defend(){
		return "jsp/view/defended";
	}
	/**
	 * 
	 * @Title: hepatitis 
	 * @Description: 肝炎 
	 * @return
	 * @author lb    
	 * @date 2019年2月16日 下午4:26:19 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/hepatitis")
	public String hepatitis(){
		return "jsp/view/hepatitis";
	}
}
