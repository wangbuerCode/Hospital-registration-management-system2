package com.xhu.controller;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xhu.entity.User;
import com.xhu.entity.ifodoctor;
import com.xhu.entity.roomlist;
import com.xhu.service.ManagerService;
import com.xhu.util.BaseAction;
import com.xhu.util.BootPage;

@Controller
@RequestMapping("/Manager")
public class MangerController extends BaseAction {

	@Autowired
	ManagerService ManSer;

	/**
	 * 
	 * @Title: informationListMa
	 * @Description: TODO(获取科室列表)
	 * @param page
	 * @return
	 * @author lb lb
	 * @date 2019年1月1日 下午4:54:28
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/roomlist")
	@ResponseBody
	public BootPage<roomlist> informationListMa(BootPage<roomlist> page, String search) {
		return ManSer.getMedicine(page, search);
	}
	
	/**
	 * 
	 * @Title: getUser 
	 * @Description: 获取用户列表 
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月26日 下午1:40:42 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/listUser")
	@ResponseBody
	public BootPage<User> getUser(BootPage<User> page, String search){
		return ManSer.listUser(page, search);
	}
	/**
	 * 
	 * @Title: roommanger
	 * @Description: TODO(修改科室信息)
	 * @return
	 * @author lb lb
	 * @date 2019年1月2日 下午4:17:15
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("update.json")
	@ResponseBody
	public ResultObj roommanger(roomlist list) {
		try {
			ManSer.update(list);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}
	/**
	 * 
	 * @Title: chongzhi 
	 * @Description: 重置密码 
	 * @param user
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月26日 下午2:32:21 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/cz.json")
	@ResponseBody
	public ResultObj chongzhi(User user) {
		try {
			ManSer.update1(user);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}
	/**
	 * 
	 * @Title: addroom
	 * @Description: TODO(增加科室)
	 * @param list
	 * @return
	 * @author lb lb
	 * @date 2019年1月3日 下午12:37:19
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("addroom")
	@ResponseBody
	public ResultObj addroom(roomlist list) {
		try {
			ManSer.addRoom(list);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}

	/**
	 * 
	 * @Title: delRoom
	 * @Description: TODO(删除科室)
	 * @param list
	 * @return
	 * @author lb lb
	 * @date 2019年1月3日 下午12:56:07
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("delroom")
	@ResponseBody
	public ResultObj delRoom(roomlist list) {
		try {
			ManSer.delroom(list);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}
	/**
	 * 
	 * @Title: deluser 
	 * @Description: 删除用户
	 * @param list
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月26日 下午2:45:41 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("deluser")
	@ResponseBody
	public ResultObj deluser(User list) {
		try {
			ManSer.deluser(list);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}
	/**
	 * 
	 * @Title: getdocifo
	 * @Description: TODO(获取医生信息)
	 * @param page
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 下午12:30:50
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/listdoctorifo")
	@ResponseBody
	public BootPage<ifodoctor> getdocifo(BootPage<ifodoctor> page, String search) {
		return ManSer.getDoctorIfo1(page, search);
	}

	/**
	 * 
	 * @Title: listroom
	 * @Description: TODO(动态加载科室信息)
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 下午2:17:12
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("listroom.json")
	@ResponseBody
	public List<roomlist> listroom() {
		return ManSer.listRoom();
	}

	/**
	 * 
	 * @Title: uodateifo
	 * @Description: TODO(修改信息)
	 * @param doc
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 下午2:40:32
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("updateifo")
	@ResponseBody
	public ResultObj uodateifo(ifodoctor doc) {
		try {
			ManSer.updateifo(doc);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}

	/**
	 * 
	 * @Title: deleteifo
	 * @Description: TODO(删除医生)
	 * @param doc
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 下午2:56:01
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("delifo")
	@ResponseBody
	public ResultObj deleteifo(ifodoctor doc) {
		try {
			ManSer.delifo(doc);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}

	/**
	 * 
	 * @Title: addroom
	 * @Description: TODO(添加医生)
	 * @param list
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 下午3:29:31
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("adddocifo")
	@ResponseBody
	public ResultObj adddocifo(ifodoctor list, @RequestParam("doctordate") String doctorEntryTime) {
		try {
			ManSer.adddocifo(list, doctorEntryTime);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultObj(FAIL);
		}
	}

	/**
	 * 
	 * @Title: getf
	 * @Description: TODO(获取发病率)
	 * @param Page
	 * @param reportperiod
	 * @return
	 * @author lb lb
	 * @throws ParseException
	 * @date 2019年1月6日 下午1:22:03
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("fabing.json")
	@ResponseBody
	public BootPage<Map<String, String>> getf(BootPage<Map<String, String>> Page, String year, String name)
			throws ParseException {
		return ManSer.getf(Page, year, name);
	}

	/**
	 * 
	 * @Title: get
	 * @Description: TODO(返回每月每种疾病的男女比列)
	 * @param Page
	 * @param reportperiod
	 * @return
	 * @throws ParseException
	 * @author lb lb
	 * @date 2019年1月6日 下午3:16:05
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("sex.json")
	@ResponseBody
	public BootPage<Map<String, String>> get(BootPage<Map<String, String>> Page, String year, String ilname)
			throws ParseException {
		return ManSer.getse(Page, year, ilname);
	}

	/**
	 * 
	 * @Title: getsexage
	 * @Description: 每个年龄段的患病人数
	 * @param Page
	 * @param reportperiod
	 * @return
	 * @throws ParseException
	 * @author lb lb
	 * @date 2019年1月6日 下午5:26:22
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("sexage.json")
	@ResponseBody
	public BootPage<Map<String, String>> getsexage(BootPage<Map<String, String>> Page, String year, String ilname)
			throws ParseException {
		return ManSer.getsexage(Page, year, ilname);
	}

	/**
	 * 
	 * @Title: listYear
	 * @Description: TODO(获取自2010年以来某中疾病的变化规律)
	 * @param Page
	 * @param reportperiod
	 * @return
	 * @throws ParseException
	 * @author lb lb
	 * @date 2019年1月7日 下午3:00:11
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("year.json")
	@ResponseBody
	public BootPage<Map<String, String>> listYear(BootPage<Map<String, String>> Page, String ilname)
			throws ParseException {
		return ManSer.getEveryYear(Page, ilname);
	}

	/**
	 * 
	 * @Title: getM
	 * @Description: TODO(获取年份)
	 * @return
	 * @author lb lb
	 * @date 2019年1月8日 上午9:15:03
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("mouth.json")
	@ResponseBody
	public HashMap<String, Object> getM() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// 月份
		map.put("list", ManSer.getMouth());
		// 学生数据
		map.put("stulist", ManSer.getS());
		map.put("stulistone", ManSer.getS1());
		map.put("stulisttwo", ManSer.getS2());
		map.put("stulistthree", ManSer.getS3());
		// 农民数据
		map.put("farmer", ManSer.getf());
		map.put("farmerone", ManSer.getf1());
		map.put("farmertwo", ManSer.getf2());
		map.put("farmethree", ManSer.getf3());
		// 白领
		map.put("white", ManSer.getwh());
		map.put("whiteone", ManSer.getwh1());
		map.put("whitetwo", ManSer.getwh2());
		map.put("whitethree", ManSer.getwh3());
		return map;
	}

	/**
	 * 
	 * @Title: getNum
	 * @Description: 获取数量和发生率
	 * @return
	 * @author lb lb
	 * @date 2019年1月11日 下午1:15:27
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("getNumAndIndex.json")
	@ResponseBody
	public HashMap<String, Object> getNum(String ilname, String year) {
		return ManSer.getNumAndIndex(ilname, year);
	}

	/**
	 * 
	 * @Title: listYearData
	 * @Description: 获得年度数据
	 * @param Page
	 * @param year
	 * @param name
	 * @return
	 * @throws ParseException
	 * @author lb lb
	 * @date 2019年3月10日 下午4:33:49
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/listYearData")
	@ResponseBody
	public BootPage<Map<String, String>> listYearData(BootPage<Map<String, String>> Page, String year, String name)
			throws ParseException {
		return ManSer.listYearData(Page,year);
	}
	
	/**
	 * 
	 * @Title: listIlname 
	 * @Description: 获取药品数据
	 * @return
	 * @author lb  lb  
	 * @date 2019年3月11日 下午7:50:43 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/Medecine.json")
	@ResponseBody
	public HashMap<String,Object> listIlname(){
		return ManSer.listilname();
	}
	/**
	 * 
	 * @Title: listIlname 
	 * @Description: 获取药品数据
	 * @return
	 * @author lb  lb  
	 * @date 2019年3月11日 下午7:50:43 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/Medecine1.json")
	@ResponseBody
	public HashMap<String,Object> listIlnameYearData(String year){
		return ManSer.listilnameYearData(year);
	}
}
