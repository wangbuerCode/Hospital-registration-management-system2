package com.xhu.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xhu.entity.checklist;
import com.xhu.entity.ifodoctor;
import com.xhu.entity.orderlist;
import com.xhu.entity.treatmentrecord;
import com.xhu.service.DoctorService;
import com.xhu.util.BaseAction;
import com.xhu.util.BootPage;

@RequestMapping("/Doctor")
@Controller
public class DoctorController extends BaseAction {

	@Autowired
	DoctorService DS;
	/**
	 * 
	 * @Title: reDocotrIfo 
	 * @Description: 医生查看
	 * @param page
	 * @return
	 * @author lb  lb  
	 * @date 2019年2月20日 下午1:47:51 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/doctor.json")
	@ResponseBody
	public BootPage<ifodoctor> reDocotrIfo(BootPage<ifodoctor> page,@RequestParam("name")String name) {
		return DS.getDoctorIfo(page,name);
	}
	/**
	 * 
	 * @Title: getOrder 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param doctorId
	 * @param doctorName
	 * @param request
	 * @param date
	 * @param id
	 * @return
	 * @author lb  lb  
	 * @date 2019年2月24日 下午4:05:45 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/order")
	@ResponseBody
	public ResultObj getOrder(String doctorId, String doctorName, HttpServletRequest request,String date,String id) {
		try {
			DS.inserOrder(doctorId, doctorName, request,date);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResultObj(FAIL);
			// TODO: handle exception
		}
	}
	/**
	 * 
	 * @Title: getorderlist 
	 * @Description: TODO(获取诊疗记录) 
	 * @param page
	 * @param request
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月29日 下午12:17:35 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/order.json")
	@ResponseBody
	public BootPage<orderlist> getorderlist(BootPage<orderlist> page, HttpServletRequest request,String search) {
		return DS.getor(page, request,search);
	}
	/**
	 * 
	 * @Title: getorderlist 
	 * @Description: 病人查看挂号纪录
	 * @param page
	 * @param request
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月29日 下午12:17:35 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/orderuser.json")
	@ResponseBody
	public BootPage<orderlist> getorderlistforuser(BootPage<orderlist> page, HttpServletRequest request) {
		return DS.getorforUser(page, request);
	}
	/**
	 * 
	 * @Title: insertTre 
	 * @Description: TODO(新建诊疗记录) 
	 * @param tre
	 * @param request
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月29日 下午12:17:12 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/trement")
	@ResponseBody
	public ResultObj insertTre(treatmentrecord tre,HttpServletRequest request,String id) {
		try {
			
			DS.insertTre(tre,request);
			DS.del(id);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResultObj(FAIL);
			// TODO: handle exception
		}
	}
	/**
	 * 
	 * @Title: newCheckList 
	 * @Description: TODO(新建检查单) 
	 * @param ck
	 * @param date
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月31日 下午4:13:50 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/check")
	@ResponseBody
	public ResultObj newCheckList(checklist ck,String date,HttpServletRequest request) {
		try {
			DS.check(ck, date,request);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResultObj(FAIL);
			// TODO: handle exception
		}
	}
	/**
	 * 
	 * @Title: checklist 
	 * @Description: TODO(获取检查列表) 
	 * @param page
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月31日 下午4:51:00 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/checklist.json")
	@ResponseBody
	public BootPage<checklist> checklist(BootPage<checklist> page,HttpServletRequest request,String search) {
		return DS.getChecklist(page,request,search);
	}
	/**
	 * 
	 * @Title: allchecklist 
	 * @Description: TODO(每个科室获取自己的检查列表) 
	 * @param page
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月31日 下午5:22:53 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/everychecklist.json")
	@ResponseBody
	public BootPage<checklist> allchecklist(BootPage<checklist> page,HttpServletRequest request,@RequestParam("name")String name) {
		return DS.allgetChecklist(page,request,name);
	}
	/**
	 * 
	 * @Title: saveResult 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param ck
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月10日 下午2:24:03 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/saveresult")
	@ResponseBody
	public ResultObj saveResult(checklist ck) {
		try {
			DS.saveRe(ck);
			return new ResultObj(SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResultObj(FAIL);
			// TODO: handle exception
		}
	}
	/**
	 * 
	 * @Title: confirmpwd 
	 * @Description: 修改密码
	 * @param userid
	 * @param pwd
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月10日 下午2:26:00 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	@RequestMapping("/updatepwd")
	@ResponseBody
	public String confirmpwd(@RequestParam("id")String id,@RequestParam("pwd")String pwd){
		if(DS.update(id, pwd)){
			return "success";
		}else{
			return "false";
		}
	}
}
