package com.xhu.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
public abstract class BaseAction {
	private Logger logger = LoggerFactory.getLogger(BaseAction.class);
	protected static final String SUCCESS = "success";
	protected static final String FAIL = "fail";

    @Autowired
    private HttpSession session;


	@InitBinder  
    protected void initBinder(WebDataBinder binder) {  
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));  
//        binder.registerCustomEditor(int.class, new CustomNumberEditor(int.class, true));  
//        binder.registerCustomEditor(int.class, new IntegerEditor());  
//        binder.registerCustomEditor(long.class, new CustomNumberEditor(long.class, true));
//        binder.registerCustomEditor(long.class, new LongEditor());  
//        binder.registerCustomEditor(double.class, new DoubleEditor());  
//        binder.registerCustomEditor(float.class, new FloatEditor());  
    } 
	
	@ExceptionHandler
	@ResponseBody
	public ResultObj exp(HttpServletRequest request, Exception ex) {
		// 璁板綍鏃ュ織
		logger.error(ex.getMessage(), ex);
		return new ResultObj(FAIL);
	}

	public final static ObjectMapper mapper = new ObjectMapper(); 
	
	public static JavaType getCollectionType(Class<?> collectionClass,
			Class<?>... parameterClasses) {
		return mapper.getTypeFactory().constructParametricType(collectionClass, parameterClasses);

	}
	/**
	 * 鑾峰彇娉涘瀷鐨凜ollection Type
	 * @param jsonStr json瀛楃涓�
	 * @param collectionClass 娉涘瀷鐨凜ollection
	 * @param parameterClasses 鍏冪礌绫诲瀷
	 */
	public static <T> T readJson(String jsonStr, Class<?> collectionClass, Class<?>... parameterClasses) throws Exception {
	       ObjectMapper mapper = new ObjectMapper();
	       JavaType javaType = mapper.getTypeFactory().constructParametricType(collectionClass, parameterClasses);
	       SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	       mapper.setDateFormat(fmt);
	       return mapper.readValue(jsonStr, javaType);

	}
	public class ResultObj {
		private String result;
		private String detail;
		private Object obj;
		
		public ResultObj(String result){
			this.result=result;
		}
		
		public ResultObj(String result,String detail) {
			this.result = result;
			this.detail = detail;
		}

		public String getResult() {
			return result;
		}

		public void setResult(String result) {
			this.result = result;
		}

		public String getDetail() {
			return detail;
		}

		public void setDetail(String detail) {
			this.detail = detail;
		}

		public Object getObj() {
			return obj;
		}

		public void setObj(Object obj) {
			this.obj = obj;
		}
	}
	
	public class DatagridMsg {
		private boolean isError = false;
		private String msg;
		
		public DatagridMsg() {
		}
		public DatagridMsg(boolean isError,String msg) {
			this.isError = isError;
			this.msg = msg;
		}
		public boolean isError() {
			return isError;
		}
		public void setError(boolean isError) {
			this.isError = isError;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
	}
	/**
	 * @Title: getCurrentUserId 
	 * @Description:鍙栧緱褰撳墠鐢ㄦ埛id
	 * @return
	 * @author 榛勮儨  hs  
	 * @date 2016骞�6鏈�29鏃� 涓嬪崍5:22:00
	 */
	public String getCurrentUserId() {
		return (String)session.getAttribute("userid");
	}
	/**
	 * @Title: getCurrentUser 
	 * @Description:鍙栧緱褰撳墠鐢ㄦ埛
	 * @return
	 * @author 榛勮儨  hs  
	 * @date 2016骞�6鏈�29鏃� 涓嬪崍5:22:14
	 */
	public Object getCurrentUser(){
		return session.getAttribute("sysuser");
	}
	public HttpSession getSession() {
		return session;
	}
	/**
	 * 
	 * @Title: readUrl 
	 * @Description: 璇诲彇url
	 * @param strUrl
	 * @return
	 * @author 鐜嬩箰  wl  
	 * @date 2016骞�7鏈�22鏃� 涓婂崍10:40:25 
	 * @淇敼淇℃伅 (淇敼浜轰慨鏀瑰唴瀹瑰強淇敼鏃堕棿)
	 */
	public static StringBuffer readUrl(String strUrl) {
		URL url = null;
		HttpURLConnection connection = null;

		try {
			url = new URL(strUrl);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.setConnectTimeout(1000*5);
			connection.connect();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream(), "utf-8"));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}
			reader.close();
			return buffer;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
}
