package com.xhu.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xhu.dao.UserMapper;
import com.xhu.dao.newsMapper;
import com.xhu.entity.User;
import com.xhu.entity.UserExample;
import com.xhu.entity.news;
import com.xhu.entity.newsExample;

@Service
public class DelLoginService {
	@Autowired
	UserMapper ifodao;
	@Autowired
	newsMapper newService;

	public void delLogin(String username, String password, HttpServletRequest request, String role) throws Exception {
		UserExample example = new UserExample();
		UserExample.Criteria criteria = example.createCriteria();
		/*
		 * ifodoctorExample ifoexample = new ifodoctorExample();
		 * ifodoctorExample.Criteria ifoCriteria = ifoexample.createCriteria();
		 */
		criteria.andUserPidEqualTo(username);
		criteria.andUserPasswordEqualTo(password);
		criteria.andUserProEqualTo(role);
		List<User> temp = ifodao.selectByExample(example);
		if (temp.isEmpty()) {
			throw new Exception("用户不存在");
		} else {
			if ("4".equals(role)) {
				HttpSession session = request.getSession();
				session.setAttribute("sys", temp.get(0).getUserName());
				session.setAttribute("sysname", temp.get(0).getUserName());
			} else if ("1".equals(role) || "2".equals(role) || "3".equals(role)) {
				HttpSession session = request.getSession();
				session.setAttribute("sys", temp.get(0).getUserPid());
				session.setAttribute("sysname", temp.get(0).getUserName());
			}
		}
		/*
		 * if (role.equals("doctor") && !"".equals(role)) { if (temp.isEmpty())
		 * { throw new Exception("用户不存在"); } else { HttpSession session =
		 * request.getSession(); session.setAttribute("sys",
		 * temp.get(0).getUserPid()); session.setAttribute("sysname",
		 * temp.get(0).getUserName()); } } else if (role.equals("user") &&
		 * !"".equals(role)) {
		 * 
		 * if ( temp.isEmpty()) { throw new Exception("用户不存在"); } else {
		 * HttpSession session = request.getSession();
		 * session.setAttribute("sys", temp.get(0).getUserName());
		 * session.setAttribute("sysname", temp.get(0).getUserName()); } }else
		 * if(role.equals("room") && !"".equals(role)){
		 * 
		 * if (temp.isEmpty()) { throw new Exception("用户不存在"); } else {
		 * HttpSession session = request.getSession();
		 * session.setAttribute("sys", temp.get(0).getUserPid());
		 * session.setAttribute("sysname", temp.get(0).getUserName()); } }else
		 * if(role.equals("manager") && !"".equals(role)){ if (temp.isEmpty()) {
		 * throw new Exception("用户不存在"); } else { HttpSession session =
		 * request.getSession(); session.setAttribute("sys",
		 * temp.get(0).getUserPid()); session.setAttribute("sysname",
		 * temp.get(0).getUserName()); } }
		 */
	}

	/**
	 * 
	 * @Title: insertId
	 * @Description: 处理用户注册
	 * @param username
	 * @param password
	 * @param idcard
	 * @author lb lb
	 * @date 2019年2月18日 下午1:21:42
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void insertId(String username, String password, String idcard) {
		User example = new User();
		if (username != null && "".equals(username)) {

		}
		example.setUserName(username);
		example.setUserPassword(password);
		example.setUserPid(idcard);
		example.setUserPro("4");
		example.setUserRoom("0");//是否完善身份信息
		ifodao.insertSelective(example);
	}

	/**
	 * 
	 * @Title: insertNews
	 * @Description: 发布心情
	 * @param context
	 * @author lb lb
	 * @date 2019年2月18日 下午1:23:24
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public boolean insertNews(String context, HttpServletRequest request) {
		news si = new news();
		if (context != null && !"".equals(context)) {
			si.setPublishContext(context);
		}
		HttpSession sis = request.getSession();
		String sysname = (String) sis.getAttribute("sys");
		si.setPublishName(sysname);
		// SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd
		// HH:mm:ss");//设置日期格式
		si.setPublishTime(new Date());// new Date()为获取当前系统时间
		int flag = newService.insert(si);
		if (flag > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @Title: listNews
	 * @Description: 获取留言的信息
	 * @return
	 * @author lb lb
	 * @date 2019年2月18日 下午2:13:51
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<news> listNews() {
		newsExample ex = new newsExample();
		// newsExample.Criteria example = ex.createCriteria();
		ex.setOrderByClause("publish_time DESC");
		List<news> list = newService.selectByExample(ex);
		return list;
	}
}
