package com.xhu.service;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.xhu.dao.analysistableMapper;
import com.xhu.dao.medicineMapper;
import com.xhu.dao.medicineMediaMapper;
import com.xhu.dao.medicineRecordMapper;
import com.xhu.dao.treatmentrecordMapper;
import com.xhu.entity.analysistable;
import com.xhu.entity.medicine;
import com.xhu.entity.medicineExample;
import com.xhu.entity.medicineMedia;
import com.xhu.entity.medicineMediaExample;
import com.xhu.entity.treatmentrecord;
import com.xhu.entity.treatmentrecordExample;
import com.xhu.util.BootPage;
import com.xhu.entity.medicineRecord;
import com.xhu.entity.medicineRecordExample;
@Service
public class MedicineService {

	@Autowired
	medicineMapper me;
	@Autowired
	medicineMediaMapper mema;
	@Autowired
	treatmentrecordMapper tre;
	@Autowired
	analysistableMapper anal;
	@Autowired
	medicineRecordMapper medicineRecord;
	/**
	 * 
	 * @Title: getMedicine
	 * @Description: TODO(获取药品清单)
	 * @param page
	 * @param name
	 * @return
	 * @author lb lb
	 * @date 2018年12月29日 下午12:15:58
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<medicine> getMedicine(BootPage<medicine> page, @RequestParam("name") String name) {
		medicineExample medi = new medicineExample();
		List<medicine> list = me.selectByExample(medi, page, name);
		page.setRows(list);
		page.setTotal(me.countByExample(name));
		medi.setPage(page);
		return page;
	}

	/**
	 * 
	 * @Title: getMedicineMd
	 * @Description: TODO(将药品清单插入中间表)
	 * @param page
	 * @return
	 * @author lb lb
	 * @date 2018年12月29日 下午12:16:25
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<medicineMedia> getMedicineMd(BootPage<medicineMedia> page) {
		medicineMediaExample medi1 = new medicineMediaExample();
		List<medicineMedia> list = mema.selectByExample(medi1, page);
		page.setRows(list);
		page.setTotal(mema.countByExample());
		medi1.setPage(page);
		return page;
	}

	/**
	 * 获取诊疗记录列表
	 * 
	 * @param page
	 * @return
	 */
	public BootPage<treatmentrecord> getTre(BootPage<treatmentrecord> page, String search) {
		treatmentrecordExample medi1 = new treatmentrecordExample();
		List<treatmentrecord> list = tre.selectByExample1(medi1, page, search);
		page.setRows(list);
		page.setTotal(tre.countByExample1(search));
		medi1.setPage(page);
		return page;
	}

	/**
	 * 
	 * @Title: parseExcel
	 * @Description: TODO(药品清单导入)
	 * @param filename
	 * @param stream
	 * @throws ParseException
	 * @author lb lb
	 * @date 2018年12月29日 下午12:15:31
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void parseExcel(String filename, InputStream stream) throws ParseException {
		mema.del();
		boolean isE2007 = false;
		if (filename.endsWith("xlsx"))
			isE2007 = true;
		InputStream input = stream;
		Workbook wb = null;
		try {
			if (isE2007)
				wb = new XSSFWorkbook(input);
			else
				wb = new HSSFWorkbook(input);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Sheet sheet = wb.getSheetAt(0);
		Iterator<Row> rows = sheet.rowIterator();
		// List<medicine> list = new ArrayList<medicine>();
		while (rows.hasNext()) {
			medicineMedia medi = new medicineMedia();
			Row row = rows.next();
			Iterator<Cell> cells = row.cellIterator();
			while (cells.hasNext()) {
				Cell cell = cells.next();
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (row.getRowNum() > 0) {
					if (cell.getColumnIndex() == 0) {
						medi.setMnoid(cell.getStringCellValue().trim());
					}
					if (cell.getColumnIndex() == 1) {
						medi.setMname(cell.getStringCellValue().trim());
					}
					if (cell.getColumnIndex() == 2) {
						medi.setMspecs(cell.getStringCellValue().trim());
					}
					if (cell.getColumnIndex() == 3) {
						medi.setMunit(cell.getStringCellValue().trim());
					}
					if (cell.getColumnIndex() == 4) {
						medi.setMprice(Integer.parseInt(cell.getStringCellValue().trim()));
					}
					if (cell.getColumnIndex() == 5) {
						Calendar c = Calendar.getInstance();
						c.set(Calendar.YEAR, 1900);
						c.set(Calendar.MONTH, 1);
						c.set(Calendar.DAY_OF_YEAR, 1);
						c.add(Calendar.DAY_OF_YEAR, Integer.parseInt(cell.getStringCellValue().trim()) - 2);
						medi.setMdate(c.getTime());
					}
					if (cell.getColumnIndex() == 6) {
						medi.setMval(Integer.parseInt(cell.getStringCellValue()));
					}
					if (cell.getColumnIndex() == 7) {
						medi.setMnumber(Integer.parseInt(cell.getStringCellValue().trim()));
					}
					if (cell.getColumnIndex() == 8) {
						medi.setSno(cell.getStringCellValue().trim());
					}
					if (cell.getColumnIndex() == 9) {
						medi.setIscf(cell.getStringCellValue().trim());
					}
				}
			}
			if (row.getRowNum() > 0) {
				mema.insertSelective(medi);
			}
		}
	}

	/**
	 * 
	 * @Title: insert
	 * @Description: TODO(将中间表数据同步到数据库)
	 * @author lb lb
	 * @date 2018年12月29日 下午12:40:35
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void insert() {
		me.insert1();
		mema.del();
	}

	/**
	 * 
	 * @Title: confirm
	 * @Description: TODO(更新库存)
	 * @param num
	 * @author lb lb
	 * @throws Exception 
	 * @date 2018年12月28日 下午4:24:44
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void confirmService(String num, String id) throws Exception {
		medicine m = this.getmedicine(id);
		//生成出库记录
		medicineRecord meR = new medicineRecord();
		int number = Integer.parseInt(num);
		meR.setMnoid(m.getMnoid());
		meR.setMname(m.getMname());
		meR.setMspecs(m.getMspecs());
		meR.setMunit(m.getMunit());
		meR.setMprice(m.getMprice());
		meR.setMdate(new Date());
		meR.setMval(m.getMval());
		meR.setMnumber(number);
		meR.setSno(m.getSno());
		meR.setIscf(m.getIscf());
		//更新库存
		if(m.getMnumber() >= number){
			medicineRecord.insert(meR);
			medicine si = new medicine();
			medicineExample medi = new medicineExample();
			medicineExample.Criteria example = medi.createCriteria();
			example.andIdEqualTo(m.getId());
			int number1 = m.getMnumber() - number;
			si.setMnumber(number1);
			me.updateByExampleSelective(si, medi);
		}else{
				throw new Exception("not en");
		}
	}

	/**
	 * 根据主键获取对象
	 * 
	 * @Title: getmedicine
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @return
	 * @author lb lb
	 * @date 2018年12月29日 下午12:54:26
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public medicine getmedicine(String id) {
		medicineExample medi = new medicineExample();
		medicineExample.Criteria example = medi.createCriteria();
		int Mid = Integer.parseInt(id);
		example.andIdEqualTo(Mid);
		return me.selectByPrimaryKey(Mid);
	}

	/**
	 * 
	 * @Title: confirm
	 * @Description: 确认疾病
	 * @return
	 * @author lb lb
	 * @date 2019年2月27日 下午3:21:02
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public boolean confirm(analysistable al) {
		al.setChecktime(new Date());
		int num = anal.insert(al);
		if (num > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @Title: getMedicine
	 * @Description: TODO(获取药品清单)
	 * @param page
	 * @param name
	 * @return
	 * @author lb lb
	 * @date 2018年12月29日 下午12:15:58
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<medicineRecord> getMedicine1(BootPage<medicineRecord> page) {
		medicineRecordExample medi4 = new medicineRecordExample();
		List<medicineRecord> list = medicineRecord.selectByExample(page);
		page.setRows(list);
		page.setTotal(medicineRecord.countByExample1());
		medi4.setPage(page);
		return page;
	}
}
