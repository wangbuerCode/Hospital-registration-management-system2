package com.xhu.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xhu.dao.UserMapper;
import com.xhu.dao.checklistMapper;
import com.xhu.dao.ifodoctorMapper;
import com.xhu.dao.orderlistMapper;
import com.xhu.dao.roomlistMapper;
import com.xhu.dao.treatmentrecordMapper;
import com.xhu.entity.checklist;
import com.xhu.entity.checklistExample;
import com.xhu.entity.ifodoctor;
import com.xhu.entity.ifodoctorExample;
import com.xhu.entity.orderlist;
import com.xhu.entity.orderlistExample;
import com.xhu.entity.roomlist;
import com.xhu.entity.roomlistExample;
import com.xhu.entity.treatmentrecord;
import com.xhu.util.BootPage;

@Service
public class DoctorService {

	@Autowired
	ifodoctorMapper ifo;
	@Autowired
	orderlistMapper order;
	@Autowired
	treatmentrecordMapper tre;
	@Autowired
	checklistMapper checklist;
	@Autowired
	roomlistMapper room;
	@Autowired
	UserMapper user;
	/**
	 * 
	 * @Title: getDoctorIfo 
	 * @Description: TODO(获取医生列表) 
	 * @param page
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月29日 下午12:19:17 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<ifodoctor> getDoctorIfo(BootPage<ifodoctor> page,String name) {
		ifodoctorExample medi = new ifodoctorExample();
		List<ifodoctor> list = ifo.selectByExample(medi, page,name);
		page.setRows(list);
		page.setTotal(ifo.countByExample());
		medi.setPage(page);
		return page;
	}
	/**
	 * 
	 * @Title: getDoctorIfo 
	 * @Description: TODO(获取检查单列表) 
	 * @param page
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月31日 下午4:52:14 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<checklist> getChecklist(BootPage<checklist> page,HttpServletRequest request,String search) {
		HttpSession session = request.getSession();
		String doctorId = (String) session.getAttribute("sys");
		checklistExample medi = new checklistExample();
		List<checklist> list = checklist.selectByExample1(medi, page,doctorId,search);
		page.setRows(list);
		page.setTotal(checklist.countByExample1(doctorId,search));
		medi.setPage(page);
		return page;
	}
	/**
	 * 
	 * @Title: inserOrder 
	 * @Description: TODO(新建预约) 
	 * @param doctorid
	 * @param doctorname
	 * @param request
	 * @author lb  lb  
	 * @throws Exception 
	 * @date 2018年12月29日 下午12:18:18 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void inserOrder(String doctorid, String doctorname, HttpServletRequest request,String date) throws Exception {
		orderlist or = new orderlist();
		//设置医生预约人数上限，设置不能重复预约
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("sys");
		int num1 = order.getAllNumber(doctorid, new SimpleDateFormat("yyyy-MM-dd").parse(date));//医生预约总人数
		int num2 = order.twonumber(doctorid, new SimpleDateFormat("yyyy-MM-dd").parse(date), username);
		if(num1 <10 && num2<1){
			or.setDoctorId(doctorid);
			or.setDoctorName(doctorname);
			or.setOrderName(username);
			or.setOrderTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
			order.insertSelective(or);
		}else{
			throw new Exception("not order");
		}
	}
	/**
	 * 
	 * @Title: getor 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param page
	 * @param request
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月31日 下午4:20:50 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<orderlist> getor(BootPage<orderlist> page,HttpServletRequest request,String search) {
		orderlistExample oderexample = new orderlistExample();
		HttpSession session = request.getSession();
		String name = (String) session.getAttribute("sys");
		List<orderlist> list = order.selectByExample1(page,name,search);
		page.setRows(list);
		page.setTotal(order.countByExample1(name,search));
		oderexample.setPage(page);
		return page;
	}
	/**
	 * 
	 * @Title: getor 
	 * @Description: 用户查看自己的挂号纪录 
	 * @param page
	 * @param request
	 * @return
	 * @author lb  lb  
	 * @date 2019年2月20日 下午2:07:32 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<orderlist> getorforUser(BootPage<orderlist> page,HttpServletRequest request) {
		orderlistExample oderexample = new orderlistExample();
		HttpSession session = request.getSession();
		String name = (String) session.getAttribute("sys");
		List<orderlist> list = order.selectByExample2(page,name);
		page.setRows(list);
		page.setTotal(order.countByExample2(name));
		oderexample.setPage(page);
		return page;
	}
	/**
	 * 
	 * @Title: insertTre 
	 * @Description: TODO(新建诊疗) 
	 * @param Tre
	 * @param request
	 * @author lb  lb  
	 * @date 2018年12月29日 下午12:19:40 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void insertTre(treatmentrecord Tre,HttpServletRequest request){
		HttpSession session = request.getSession();
		String name = (String) session.getAttribute("sys");
		Tre.setRecordTime(new Date());
		Tre.setDoctorId(name);
		tre.insertSelective(Tre);
	}
	/**
	 * 
	 * @Title: del 
	 * @Description: TODO(删除诊疗记录) 
	 * @param id
	 * @author lb  lb  
	 * @date 2018年12月31日 上午11:43:53 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public int del(String id){
		int Mid = Integer.parseInt(id);
		return order.deleteByPrimaryKey(Mid);
	}
	/**
	 * 
	 * @Title: check 
	 * @Description: TODO(新建检查单) 
	 * @param ck
	 * @param date
	 * @author lb  lb  
	 * @throws ParseException 
	 * @date 2018年12月31日 下午4:15:06 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void check(checklist ck,String date,HttpServletRequest request) throws ParseException{
		HttpSession session = request.getSession();
		String name = (String) session.getAttribute("sys");
		ck.setDoctorId(name);
		ck.setChecktime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
		checklist.insert(ck);
	}
	/**
	 * 
	 * @Title: allgetChecklist 
	 * @Description: (每个科室获取自己的检查列表) 
	 * @param page
	 * @param request
	 * @return
	 * @author lb  lb  
	 * @date 2018年12月31日 下午5:24:48 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<checklist> allgetChecklist(BootPage<checklist> page,
			HttpServletRequest request,String searchname) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String name = (String) session.getAttribute("sys");
		roomlistExample tem = new  roomlistExample();
		roomlistExample.Criteria si = tem.createCriteria();
		si.andRoomIdEqualTo(name);
		List<roomlist> temp = room.selectByExample(tem);
		String roomname = temp.get(0).getRoomName();
		checklistExample am = new checklistExample();
		checklistExample.Criteria example = am.createCriteria();
		example.andCheckroomEqualTo(roomname);
		List<checklist> allchecklist = checklist.selectByExample(page,am,searchname);
		page.setRows(allchecklist);
		page.setTotal(checklist.countByExample(am));
		am.setPage(page);
		return page;
	}
	/**
	 * 
	 * @Title: saveRe 
	 * @Description: TODO(保存结果) 
	 * @param ck
	 * @author lb  lb  
	 * @date 2018年12月31日 下午6:05:47 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void saveRe(checklist ck){
		checklist.updateByPrimaryKeySelective(ck);
	}
	/**
	 * 
	 * @Title: update 
	 * @Description: 修改密码
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月10日 下午2:33:43 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public boolean update(String pid,String pwd){
		int num = user.uppwd(pid, pwd);
		if(num > 0){
			return true;
		}else{
			return false;
		}
	}
}
