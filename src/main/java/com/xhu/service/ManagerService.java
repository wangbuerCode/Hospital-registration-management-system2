package com.xhu.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xhu.dao.UserMapper;
import com.xhu.dao.analysistableMapper;
import com.xhu.dao.ifodoctorMapper;
import com.xhu.dao.medicineRecordMapper;
import com.xhu.dao.roomlistMapper;
import com.xhu.entity.User;
import com.xhu.entity.UserExample;
import com.xhu.entity.ifodoctor;
import com.xhu.entity.ifodoctorExample;
import com.xhu.entity.roomlist;
import com.xhu.entity.roomlistExample;
import com.xhu.util.BootPage;

@Service
public class ManagerService {

	@Autowired
	roomlistMapper room;
	@Autowired
	ifodoctorMapper ifo;
	@Autowired
	analysistableMapper analysis;
	@Autowired
	UserMapper user;
	@Autowired
	medicineRecordMapper mR;

	/**
	 * 
	 * @Title: getMedicine
	 * @Description: TODO(获取科室列表的信息)
	 * @param page
	 * @return
	 * @author lb lb
	 * @date 2019年1月1日 下午5:01:13
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<roomlist> getMedicine(BootPage<roomlist> page, String search) {
		roomlistExample roomExample = new roomlistExample();
		List<roomlist> list = room.selectByExample1(roomExample, page, search);
		page.setRows(list);
		page.setTotal(room.countByExample1(search));
		roomExample.setPage(page);
		return page;
	}
	/**
	 * 
	 * @Title: listUser 
	 * @Description: 获取用户列表 
	 * @param page
	 * @param search
	 * @return
	 * @author lb  lb  
	 * @date 2019年5月26日 下午1:11:58 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<User> listUser(BootPage<User> page, String search){
		UserExample userExample = new UserExample();
		List<User> listuser = user.selectByExample1(userExample, page, search);
		page.setRows(listuser);
		page.setTotal(user.countByExample1(search));
		userExample.setPage(page);
		return page;
	}
	/**
	 * 
	 * @Title: update
	 * @Description: TODO(修改科室信息)
	 * @param rl
	 * @author lb lb
	 * @date 2019年1月2日 下午4:18:36
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void update(roomlist rl) {
		room.updateByPrimaryKeySelective(rl);
	}
	/**
	 * 
	 * @Title: update1 
	 * @Description: 重置密码 
	 * @param rl
	 * @author lb  lb  
	 * @date 2019年5月26日 下午2:35:12 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void update1(User rl) {
		rl.setUserPassword("66666666");
		user.updateByPrimaryKeySelective(rl);
	}
	/**
	 * 
	 * @Title: addRoom
	 * @Description: TODO(增加科室)
	 * @param list
	 * @author lb lb
	 * @date 2019年1月3日 下午12:38:04
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void addRoom(roomlist list) {
		// TODO Auto-generated method stub
		room.insertSelective(list);
	}

	/**
	 * 
	 * @Title: delroom
	 * @Description: TODO(删除科室)
	 * @param list
	 * @author lb lb
	 * @date 2019年1月3日 下午12:56:33
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void delroom(roomlist list) {
		// TODO Auto-generated method stub
		room.deleteByPrimaryKey(list.getId());
	}
	public void deluser(User users) {
		// TODO Auto-generated method stub
		user.deleteByPrimaryKey(users.getUserId());
	}
	/**
	 * 
	 * @Title: getDoctorIfo1
	 * @Description: TODO(获取医生信息)
	 * @param page
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 下午12:54:32
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<ifodoctor> getDoctorIfo1(BootPage<ifodoctor> page, String search) {
		ifodoctorExample medi = new ifodoctorExample();
		List<ifodoctor> list = ifo.selectByExample1(medi, page, search);
		page.setRows(list);
		page.setTotal(ifo.countByExample1(search));
		medi.setPage(page);
		return page;
	}

	/**
	 * 
	 * @Title: listRoom
	 * @Description: TODO(动态加载科室)
	 * @return
	 * @author lb lb
	 * @date 2019年1月4日 下午2:15:00
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<roomlist> listRoom() {
		roomlistExample ex = new roomlistExample();
		return room.selectByExample(ex);
	}

	/**
	 * 修改医生信息
	 * 
	 * @Title: updateifo
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param doc
	 * @author lb lb
	 * @date 2019年1月4日 下午2:41:53
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void updateifo(ifodoctor doc) {
		// TODO Auto-generated method stub
		ifo.updateByPrimaryKeySelective(doc);
	}

	/**
	 * 删除医生
	 * 
	 * @Title: delifo
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param doc
	 * @author lb lb
	 * @date 2019年1月4日 下午2:56:41
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void delifo(ifodoctor doc) {
		// TODO Auto-generated method stub
		ifo.deleteByPrimaryKey(doc.getId());
	}

	/**
	 * 
	 * @Title: adddocifo
	 * @Description: TODO(添加医生)
	 * @param list
	 * @param doctorEntryTime
	 * @author lb lb
	 * @throws ParseException
	 * @date 2019年1月4日 下午3:31:14
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public void adddocifo(ifodoctor list, String doctorEntryTime) throws ParseException {
		list.setDoctorEntryTime(new SimpleDateFormat("yyyy-MM-dd").parse(doctorEntryTime));
		list.setExitStatu("1");
		User users = new User();
		users.setUserName(list.getDoctorName());
		users.setUserPid(list.getDoctorId());
		users.setUserPassword("888888");
		users.setUserPro("2");
		ifo.insertSelective(list);
		user.insertSelective(users);
	}

	/**
	 * 
	 * @Title: getf
	 * @Description: TODO(获取发病率)
	 * @param page
	 * @param reportperiod
	 * @return
	 * @author lb lb
	 * @throws ParseException
	 * @date 2019年1月6日 下午1:16:39
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<Map<String, String>> getf(BootPage<Map<String, String>> page, String reportperiod, String name)
			throws ParseException {
		List<Map<String, String>> list = analysis.listFa(reportperiod, name);
		page.setRows(list);
		page.setTotal(list.size());
		return page;
	}

	/**
	 * 
	 * @Title: getse
	 * @Description: TODO(获取每月每种疾病的男女比列)
	 * @param page
	 * @param reportperiod
	 * @return
	 * @throws ParseException
	 * @author lb lb
	 * @date 2019年1月6日 下午3:14:31
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<Map<String, String>> getse(BootPage<Map<String, String>> page, String reportperiod, String ilname)
			throws ParseException {
		List<Map<String, String>> list = analysis.getsex(reportperiod, ilname);
		page.setRows(list);
		page.setTotal(list.size());
		return page;
	}

	/**
	 * 
	 * @Title: getsexage
	 * @Description: TODO(每个年龄段的患病人数)
	 * @param page
	 * @param reportperiod
	 * @return
	 * @throws ParseException
	 * @author lb lb
	 * @date 2019年1月6日 下午5:25:26
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<Map<String, String>> getsexage(BootPage<Map<String, String>> page, String reportperiod,
			String ilname) throws ParseException {
		List<Map<String, String>> list = analysis.getagesex(reportperiod, ilname);
		page.setRows(list);
		page.setTotal(list.size());
		return page;
	}

	/**
	 * 
	 * @Title: getEveryYear
	 * @Description: TODO(获取每一年某种疾病的变化)
	 * @param page
	 * @param reportperiod
	 * @return
	 * @throws ParseException
	 * @author lb lb
	 * @date 2019年1月7日 下午2:59:16
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<Map<String, String>> getEveryYear(BootPage<Map<String, String>> page, String reportperiod)
			throws ParseException {
		List<Map<String, String>> list = analysis.getEveryYear(reportperiod);
		page.setRows(list);
		page.setTotal(list.size());
		return page;
	}

	/**
	 * 
	 * @Title: getMouth
	 * @Description: TODO(获取月份)
	 * @return
	 * @author lb lb
	 * @date 2019年1月8日 上午9:30:36
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getMouth() {
		// TODO Auto-generated method stub
		return analysis.getMouth();
	}

	/**
	 * 
	 * @Title: getS
	 * @Description: TODO(获取2019年每月患感冒的学生人数)
	 * @return
	 * @author lb lb
	 * @date 2019年1月8日 上午11:14:50
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getS() {
		return analysis.getstudent();
	}

	/**
	 * 
	 * @Title: getS1
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:14:27
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getS1() {
		return analysis.getstudent1();
	}

	/**
	 * 
	 * @Title: getS2
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:14:36
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getS2() {
		return analysis.getstudent2();
	}

	/**
	 * 
	 * @Title: getS3
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:14:42
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getS3() {
		return analysis.getstudent3();
	}

	/**
	 * 
	 * @Title: getf
	 * @Description: 获取2019年患感冒的农民
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:32:17
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getf() {
		return analysis.getfarmer();
	}

	/**
	 * 
	 * @Title: getf1
	 * @Description: 获取2019年患感冒的农民
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:33:32
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getf1() {
		return analysis.getfarmer();
	}

	/**
	 * 
	 * @Title: getf2
	 * @Description: 获取2019年患感冒的农民
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:33:36
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getf2() {
		return analysis.getfarmer();
	}

	/**
	 * 
	 * @Title: getf3
	 * @Description: 获取2019年患感冒的农民
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:33:40
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getf3() {
		return analysis.getfarmer();
	}

	/**
	 * 
	 * @Title: getwh
	 * @Description: 获取2016-2019年患感冒的白领
	 * @return
	 * @author lb lb
	 * @date 2019年1月10日 下午1:52:38
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public List<Map<String, String>> getwh() {
		return analysis.getwhite();
	};

	public List<Map<String, String>> getwh1() {
		return analysis.getwhite1();
	};

	public List<Map<String, String>> getwh2() {
		return analysis.getwhite2();
	};

	public List<Map<String, String>> getwh3() {
		return analysis.getwhite3();
	};

	/**
	 * 
	 * @Title: getNumAndIndex
	 * @Description: 统计数量和发病率
	 * @return
	 * @author lb lb
	 * @date 2019年1月11日 下午1:12:42
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public HashMap<String, Object> getNumAndIndex(String ilname, String year) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("NumAndIndexlist", analysis.getoilNumberAndIndex(ilname, year));
		return map;
	}

	/**
	 * 
	 * @Title: listYearData
	 * @Description: 获得年度报表数据
	 * @return
	 * @author lb lb
	 * @date 2019年3月10日 下午4:30:44
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public BootPage<Map<String, String>> listYearData(BootPage<Map<String, String>> page, String year) {
		List<Map<String, String>> list = analysis.yearData(year);
		page.setRows(list);
		page.setTotal(list.size());
		return page;
	}
	/**
	 * 
	 * @Title: listilname 
	 * @Description: 获取药品数据
	 * @return
	 * @author lb  lb  
	 * @date 2019年3月11日 下午7:49:24 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public HashMap<String, Object> listilnameYearData(String year) {
		List<Map<String, String>> list = analysis.yearData(year);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ilnamedata1", list);
		return map;
	}
	/**
	 * 
	 * @Title: listilname 
	 * @Description: 获取药品数据
	 * @return
	 * @author lb  lb  
	 * @date 2019年3月11日 下午7:49:24 
	 * @修改信息 (修改人修改内容及修改时间)
	 */
	public HashMap<String, Object> listilname() {
		List<Map<String, String>> list = mR.ilnum();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ilnamedata", list);
		return map;
	}
}
